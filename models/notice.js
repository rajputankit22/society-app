var mongoose = require('mongoose');
var NoticeSchema = mongoose.Schema({

    subject:{
        type: String,
        required: true
    },

    content:{
        type: String,
        required: true
    },

    dateCreated: {
        type: Date,
        default: Date.now
    },
    reminderDate: { // is actually publish date now wrt issue #33
        type: Date,
        default: Date.now
    },
    societyId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Society',
        required: true
    },

    createdBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        require: true
    },

    targetAt: {  // n.b: targetAt == null (or udnefined) implies targeted at all scoiety
      flatNo: String,
      block: String
    }
});

NoticeSchema.virtual('isPublished').get(function () {
  var today = (new Date()).getTime();
  return (today >= this.reminderDate.getTime());
});

NoticeSchema.set('toJSON', {virtuals: true});
NoticeSchema.set('toObject', {virtuals: true});

module.exports = mongoose.model('Notice', NoticeSchema);
