var mongoose = require('mongoose');

var BillSchema = new mongoose.Schema({
  billNo: {
    type: String,
    required: true
  },

  societyId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Society',
    required: true
  },

  addedBy: { // admin
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },

  addedOn: {
    type: Date,
    default: Date.now()
  },

  paidBy: { // resident
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },

  serviceRequest: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'ServiceRequest'
  },

  category: String,

  block: String,
  flatNo: {type: String, default: '2.0'},

  dueDate: {type: Date, required: true},

  desc: String,

  status: String,

  amount: Number,

  paymentConfirmationNo: String,

  paymentDateTime: Date,

  paymentMode: String,

  schedulePickTime: Date

})

module.exports = mongoose.model('Bill', BillSchema);
