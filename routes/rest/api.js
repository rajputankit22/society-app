var express = require('express');

var fs = require('fs');
var randomString = require("randomstring");

// for authentication purpose
// var  passport  =  require('passport');

var  config  =  require('../../config/main')[process.env.NODE_ENV || 'development'];
var  jwt  =  require('jsonwebtoken');  

var middleware = require('../../middle.js');

// load models
var User = require('../../models/user');
var Society = require('../../models/society');
var Notice = require('../../models/notice');
var EmergencyContact = require('../../models/emergencycontact');
var BoardMember = require('../../models/boardmember');
var ResidentEmployee = require('../../models/employee');
var FrequentVisitor = require('../../models/frequentvisitor');
var OpinionPoll = require('../../models/opinionpoll');
var ResidentDocument = require('../../models/documents');
var Classified = require('../../models/classified');
var Contact = require('../../models/contact');
var ServiceOffered = require('../../models/serviceoffered');
var ServiceProvider = require('../../models/serviceprovider');
var ServiceRequest = require('../../models/servicerequest');
var Bill = require('../../models/bill');


var auth = function (role) {
    return function (req, res, next) {
      var token = req.headers.authorization || req.query.token || req.body.token;
      if (token == undefined) {
        switch (role) {
        case 'Super Admin':
          return res.json({
            error: true,
            message: "Authentication Failed"
          });
          break;
        case 'Admin':
          return res.json({
            error: true,
            message: "Authentication Failed"
          });
          break;
        case 'Resident':
          return res.json({
            error: true,
            message: "Authentication Failed"
          });
          break;
        default:
          return res.json({
            error: true,
            message: "You are not authorized"
          });
        }
      }
      if (typeof role == "string") {
        jwt.verify(token, config.secret, {
          algorithms: ["HS256"]
        }, function (err, user) {
          if (err) {
            return res.json({
              error: true,
              message: "Invalid Request"
            });
          } else if (user.role == role) {
            res.locals.user = user; // store authenticated user info
            next();
          } else {
            return res.json({
              error: true,
              message: "Authentication Failed"
            });
          }
        });
      } else if (role instanceof Array) {

        jwt.verify(token, config.secret, {
          algorithms: ["HS256"]
        }, function (err, user) {
          if (err) {
            return res.json({
              error: true,
              message: "Invalid Request"
            });
          } else if (user.role == role[0] || user.role == role[1] || user.role == role[2]) {
            res.locals.user = user; // store authenticated user info
            next();
          } else {
            return res.json({
              error: true,
              message: "Authentication Failed"
            });
          }
        });

      } else {
        return res.json({
            error: true,
            message: "Authentication Failed"
          });
      }
    }
  }





  // passport stuffs

  var apiRoutes  =  express.Router();



    apiRoutes.post('/adduser', auth('Super Admin'), function(req, res) {
      var user = new User(req.body);
      user.save(function(err) {
        if (err) {
          console.log(err);
          res.json({
            error: true,
            message: "Fill all the mandatory fields"
          });
        } else {
          res.json({
            error: false,
            data: {
              id: user._id
            }
          });
        }
      });
    });

  apiRoutes.post('/register', function (req, res, next) {
    console.log(req.body);
    // if (!req.body.contactEmail || !req.body.password) {
    //   res.json({
    //     error: true,
    //     message: 'Please enter an email and password to register'
    //   });
    // } else {
      var user = new User(req.body);

      // saving to database

      user.save(function (err) {
        if (err) {
          console.log(err);
          return res.json({
            error: true,
            message: err.errors
          });
        }
        res.json({
          error: false,
          data: {
            id: user.id
          }
        });
      });

  });

  apiRoutes.put('/editUser/:userId', auth('Super Admin'), function(req, res){
    var userId = req.params.userId;
    User.findByIdAndUpdate(userId, req.body, {new: true}, function (err, user){
      if(err){
        return res.json({
          error: true,
          message: "Could not be updated"
        });
      } else {
        res.json({
          error: false,
          data: user
        });
      }
    });
  });

  /**
     * @api {post} /authenticate Authenticate a Resident or Admin
     * @apiVersion 1.0.0
     * @apiName authenticate
    *  @apiGroup Authentication
     * @apiPermission Anyone except Superadmin
     *
     * @apiDescription Authenticates a Resident or Admin using login credentials. On success, sends JWT token plus basic user info. Should be the first endpoint a REST client should hit. All subsequent requests by the client must include the token obtained here for authentication & authorization.
     *
     * @apiParam {String} contactEmail Login Credential (Email) of user (required)
     * @apiParam {String} password Plaintext Password of user (required)
     *
     * @apiParamExample {json} Authenticate-Example:
    *  {
         "contactEmail": "foo@bar.com",
         "password": "topsecret"
       }
     * @apiSuccess {String} token        The JWT Token
     * @apiSuccess {Object} user         Basic Info of the Authenticated User
     * @apiSuccess {Object} aws          Required details for connecting to AWS S3 API; includes TEMPORARY credentials
     *
     * @apiSuccessExample {json}  Success-Response:
     *  {
          "error": false,
          "message": "Successfully Authenticated",
          "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ"
          "user": {
            "id": "1234abcd6789",
            "role": "Resident",
            "profilePicUrl": "https://s3-ap-southeast-1.amazonaws.com/my-bucket/579abcffcbe18ca33fc12345",
            "societyId": "57a1975f6b743fb034012345",
            "name": "John Doe",
            "contactEmail": "foo@bar.com",
            "block": "A-II",
            "flatNo": "L13"
          },
          aws: {
            region: 'ap-southeast-1',
            s3Endpoint: 's3-ap-southeast-1.amazonaws.com',
            imageBucketName: 'smartsociety-images',
            documentBucketName: 'smartsociety-documents',
            tempCreds: {
              AccessKeyId: '--------',
              SecretAccessKey: '--------',
              SessionToken: '--------',
              Expiration: '2016-08-25T19:34:42.000Z'
             }
           }
     *  }
     */
  apiRoutes.post('/authenticate', middleware.awsGetTempCreds, function (req, res) {
    if (!req.body.contactEmail || !req.body.password) {
      return res.json({
        error: true,
        message: 'Invalid Details'
      });
    } else {
      User.findOne({
        contactEmail: req.body.contactEmail
      }, function (err, user) {
        if (err)
          throw err;
        if (!user) {
          return res.json({
            error: true,
            message: 'You are not authorized'
          });
        } else {
          // don't allow deactivated (soft delete) residents to login
          if (user.softDeleted !== undefined && user.softDeleted === true) {
            return res.json({error: true, message: "You are Deactivated!"});
          }
          // don't allow residents of a inactive scoiety to login
          if (user.societyInactive !== undefined && user.societyInactive === true) {
            return res.json({error: true, message: "Your Society is Inactive!"});
          }
          // password checking
          user.comparePassword(req.body.password, function (err, isMatch) {
            if (isMatch && !err) {

              var payload = {
                id: user._id,
                name: user.name,
                role: user.role,
                contactEmail: user.contactEmail,
                societyId: user.societyId
              };
              //console.log(payload);
              var token = jwt.sign(payload, config.secret, {
                expiresIn: 3600*24*30 // in seconds
              });
              //console.log(token);
              res.json({
                error: false,
                message: 'Successfully Authenticated',
                token: token,
                user: {
                  id: payload.id,
                  role: payload.role,
                  profilePicUrl: user.awsProfilePicURL,
                  societyId: user.societyId,
                  name: user.name,
                  contactEmail: user.contactEmail,
                  block: user.block,
                  flatNo: user.flatNo
                },
                aws: {
                  region: config.aws.region,
                  s3Endpoint: config.aws.s3Endpoint,
                  imageBucketName: config.aws.imageBucketName,
                  documentBucketName: config.aws.documentBucketName,
                  tempCreds: res.locals.tempCreds
                }

              });
            } else {
              // password does not match
              res.json({
                error: true,
                message: 'Failed to authenticate'
              });
            }
          });
        }
      });
    }
  });

apiRoutes.post('/authenticateSuper', function (req, res) {
    if (!req.body.contactEmail || !req.body.password) {
      return res.json({
        error: true,
        message: 'Invalid Details'
      });
    } else {
      User.findOne({
        contactEmail: req.body.contactEmail,
        role: req.body.role
      }, function (err, user) {
        if (err)
          throw err;
        if (!user) {
          res.json({
            error: true,
            message: 'You are not authorized'
          });
        } else {
          // password checking
          user.comparePassword(req.body.password, function (err, isMatch) {
            if (isMatch && !err) {

              var payload = {
                id: user._id,
                name: user.name,
                role: user.role,
                contactEmail: user.contactEmail
              };
              //console.log(payload);
              var token = jwt.sign(payload, config.secret, {
                expiresIn: 3600*24*30 // in seconds
              });
              //console.log(token);
              res.json({
                error: false,
                message: 'Successfully Authenticated',
                token: token
              });
            } else {
              // password does not match
              res.json({
                error: true,
                message: 'Failed to authenticate'
              });
            }
          });
        }
      });
    }
  });

  // without protection route
  apiRoutes.get('/getUser/:userId', auth('Super Admin') ,function (req, res) {
    User.findById( req.params.userId, function (err, data) {
        if( err) {
          console.log(err);
          return res.json({
            error: true,
            message: "Could not find the user"
          });
      } else {
        res.json({
          error: false,
          data: data
        });
      }
    });
  });





  /**
     * @api {post} /addsociety Create a new Society
     * @apiVersion 1.0.0
     * @apiName addSociety
     * @apiGroup Manage Societies
     * @apiPermission Super Admin
     *
     * @apiDescription Add a new society with all the necessary details by the super admin
     *
     * @apiParam {String} societyName Name of the society (required)
     * @apiParam {String} societyAddress Detailed address of the society (required)
     * @apiParam {enum}   status=active active/inactive default: active
     * @apiParam {String} [locLat] Lattitude
     * @apiParam {String} [locLong] Longitude
     * @apiParam {String} contactPerson (Required)
     * @apiParam {String} [contactEmail]
     * @apiParam {Array(Number)} contactPhone (Required)
     * @apiParam {Array(String)} modulesSubscribed
     * @apiParam {Number} approvedPAX
     * @apiParam {Number} merchId
     * @apiParam {Number} merchKey
     * @apiParam {String} merchSalt
     * @apiParam {Array(String)} services
     *
     * @apiParamExample {json} Add-Society-Example:
    *  {
         "societyName": "Obhishikta",
         "societyAddress": "Kalyani",
         "contactPerson": "Sandeep Acharya",
         "contactEmail": "dsdsjk@kdsjsk.com",
         "locLat": 8979.44,
         "locLong": 8798798.4,
         "approvedPAX": 897789,
         "merchSalt": "6454",
         "merchKey": "879498",
         "services": [
           "ksdljjksd",
           "jsdksdj"
         ],
         "modulesSubscribed": [
           "hshdjhds",
           "kdjfolskfj"
         ],
         "contactPhone": [
           7897897897,
           9879874155
         ]
       }
     * @apiSuccess {String} id         The new society ID.
     *
     *
     */

  apiRoutes.post('/addsociety', auth('Super Admin'), function (req, res, next) {

    // if (!req.body.societyName ||
    //   !req.body.societyAddress ||
    //   !req.body.contactPerson ||
    //   !req.body.contactPhone
    // ) {
    //   return res.json({
    //     error: true,
    //     message: 'Fill all the mandatory details'
    //   });
    // } else {
      var society = new Society(req.body);

      society.save(function (err) {
        if (err) {
          console.log(err);
          return res.json({
            error: true,
            message: 'Fill all the details'
          });
        }
        res.json({
          error: false,
          data: {
            id: society.id
          }
        });
      });

  });


  /**
     * @api {get} /getsocietydetails/:id Read data of a Society
     * @apiVersion 1.0.0
     * @apiName getSocietyDetails
     * @apiGroup Manage Societies
     * @apiPermission Super Admin, Admin
     *
     * @apiDescription Get the details of the Society
     *
     *
     * @apiParam {String} id The unique Society id
     *
     *
     * @apiSuccess {String} societyName
       @apiSuccess {String} societyAddress
       @apiSuccess {String} locLat
       @apiSuccess {String} locLong
       @apiSuccess {String} contactPerson
       @apiSuccess {String} contactEmail
       @apiSuccess {Array(String)} contactPhone
       @apiSuccess {Array(String)} modulesSubscribed
       @apiSuccess {Number} approvedPAX
     *
     * @apiError notFound   The <code>id</code> of the Society was not found.
 */
  apiRoutes.get('/getsocietydetails/:id', auth(['Super Admin', 'Admin']), function (req, res, next) {
    var id = req.params.id;
    Society.findById(id, function (err, society) {
      if (err) {
        return res.json({
          error: true,
          data: 'No society found with this id'
        });
      } else {
        return res.json({
          error: false,
          data: society
        });
      }
    });
  });

  /**
     * @api {put} /editsociety/:id Update the Society
     * @apiVersion 1.0.0
     * @apiName editSociety
     * @apiGroup Manage Societies
     * @apiPermission Super Admin, Admin
     *
     * @apiDescription Update the society details, all properties must be sent
     *
     *
     * @apiParam {String} id The unique Society id (through URL)
     *

     * @apiParam {String} societyName Name of the society
     * @apiParam {String} societyAddress Detailed address of the society
     * @apiParam {String} locLat Lattitude
     * @apiParam {String} locLong Longitude
     * @apiParam {String} contactPerson
     * @apiParam {String} contactEmail
     * @apiParam {Array(Number)} contactPhone
     * @apiParam {Array(String)} modulesSubscribed
     * @apiParam {Number} approvedP
     * @apiParam {Number} merchId
     * @apiParam {Number} merchKey
     * @apiParam {String} merchSalt
     * @apiParamExample {json} Request Update Format
     * {
         "societyName": "Obhishikta",
         "societyAddress": "Kalyani",
         "contactPerson": "Sandeep Acharya",
         "contactEmail": "dsdsjk@kdsjsk.com"
    }
     @apiSuccess {json} society Newly updated society object with the properties
     @apiSuccessExample {json} Success-Response:
     {
  "error": false,
  "data": {
    "_id": "5785e7b5b7585d9426694023",
    "societyName": "Obhishikta",
    "societyAddress": "Kalyani",
    "contactPerson": "Sandeep Acharya",
    "contactEmail": "i.am.sandeep.acharya@gmail.com",
    "locLat": "8979.44",
    "locLong": "8798798.4",
    "approvedPAX": 897789,
    "merchKey": 879498,
    "merchSalt": "6454",
    "__v": 0,
    "services": [
      "ksdljjksd",
      "jsdksdj"
    ],
    "modulesSubscribed": [
      "hshdjhds",
      "kdjfolskfj"
    ],
    "contactPhone": [
      7897897897,
      9879874155
    ],
    "status": "active",
    "joinDate": "2016-07-13T07:03:17.214Z"
  }
}
 */

  apiRoutes.put('/editsociety/:id', auth(['Super Admin', 'Admin']), function (req, res) {
    var id = req.params.id;
    console.log(req.body);
    Society.findByIdAndUpdate(id, req.body, {
      new: true
    }, function (err, society) {
      if (err) {
        console.log(err);
        return res.json({
          error: true,
          message: 'Update was not possible'
        })
      } else {
        res.json({
          error: false,
          data: society
        });
      }
    });
  });

  /*
    /**
     * @api {get} /getsocietylist Get all the societies
     * @apiVersion 1.0.0
     * @apiName getSocietyList
     * @apiGroup Manage Societies
     * @apiPermission Super Admin
     *
     * @apiDescription Get all the societies brief details
     * @apiSuccess {Array(Society)} Breif society information
     *  @apiSuccess {String} id
        @apiSuccess {String} societyName
        @apiSuccess {String} societyAddress
        @apiSuccess {String} contactPerson
        @apiSuccess {String} contactEmail
        @apiSuccess {Array(Number)} contactPhone
        @apiSuccessExample {json} Success-Response:

           {
  "error": false,
  "data": [
    {
      "id": "5785d1c521e3f068239e1d5d",
      "societyName": "Urbana",
      "societyAddress": "Kolkata",
      "contactPerson": "Sandeep Acharya",
      "contactEmail": "dsdsjk@kdsjsk.com",
      "contactPhone": [
        7897897897,
        9879874155
      ]
    },
    {
      "id": "5785e7b5b7585d9426694023",
      "societyName": "Obhishikta",
      "societyAddress": "Kalyani",
      "contactPerson": "Sandeep Acharya",
      "contactEmail": "i.am.sandeep.acharya@gmail.com",
      "contactPhone": [
        7897897897,
        9879874155
      ]
    }
  ]
}
     */
  apiRoutes.get('/getsocietylist', auth('Super Admin') ,function (req, res) {
    Society.find({}, function (err, societies) {
      if (err) {
        console.log(err);
        return res.json({
          error: true,
          message: "Nothing found"
        });
      } else {
        var societyMap = societies.map(function (society) {
          var rObj = {};
          rObj.id = society._id;
          rObj.societyName = society.societyName;
          rObj.societyAddress = society.societyAddress;
          rObj.contactPerson = society.contactPerson;
          rObj.contactEmail = society.contactEmail;
          rObj.contactPhone = society.contactPhone;
          rObj.joinDate = society.joinDate;
          rObj.status = society.status;
          return rObj;

        });
        res.json({
          error: false,
          data: societyMap
        });
      }
    });
  });

  /**
     * @api {post} /addnotice Create a new Notice
     * @apiVersion 1.0.0
     * @apiName addNotice
     * @apiGroup Notice
     * @apiPermission Admin
     *
     * @apiDescription Add a new notice with all the necessary details by the  admin

     * @apiParam {String} subject   Notice Subject (Required)
     * @apiParam {String} content   Notice Content (Required)
     * @apiParam {Date} dateCreated
     * @apiParam {Date} reminderDate
     * @apiParam {String} societyId (Required)
     * @apiParam {String} createdBy Admin Id (Required)

       @apiSuccess {String} id Id of the newly created notice

     */

  apiRoutes.post('/addnotice', auth('Admin'), function (req, res) {
    var notice = new Notice(req.body);
    notice.save(function (err) {
      if (err) {
        console.log(err);
        return res.json({
          error: true,
          message: "Fill the mandatory fields"
        });
      } else {
        res.json({
          error: false,
          data: {
            id: notice.id
          }
        });
      }
    });
  });

  /**
     * @api {get} /getnotices/:societyId Get notices of a society
     * @apiVersion 1.0.0
     * @apiName getNotices
     * @apiGroup Notice
     * @apiPermission Admin, Resident
     *
     * @apiDescription Get all the notices of a particular society
     * @apiParam {String} societyId (Required)
     *  @apiSuccess {String} subject   Notice Subject
     * @apiSuccess {String} content   Notice Content
     * @apiSuccess {Date} dateCreated
     * @apiSuccessExample {json} Success Response
     {
  "error": false,
  "data": [
    {
      "subject": "Wow",
      "content": "Hiij kjefds kjdfkjas joeiwjvoew ejfojeow",
      "dateCreated": "2016-07-14T06:36:43.088Z"
    },
    {
      "subject": "jksdksjw",
      "content": "kjdshkjsdhksjdlsdla",
      "dateCreated": "2016-07-14T06:37:02.238Z"
    }
  ]
}

     */

  apiRoutes.get('/getnotices/:societyId', auth(['Admin', 'Resident']), function (req, res) {
    var societyId = req.params.societyId;

    Notice.find({
      "societyId": societyId
    }, function (err, notices) {
      if (err) {
        console.log(err);
        return res.json({
          error: true,
          message: "No notices found"
        });
      } else {
        var noticesMap = notices.map(function (notice) {
          var rObj = {};
          rObj['subject'] = notice.subject;
          rObj['content'] = notice.content;
          rObj['dateCreated'] = notice.dateCreated;
          return rObj;
        });

        res.json({
          error: false,
          data: noticesMap
        });

      }
    });
  });

  /**
     * @api {put} /editnotice/:noticeId Update a notice
     * @apiVersion 1.0.0
     * @apiName editNotice
     * @apiGroup Notice
     * @apiPermission Admin
     *
     * @apiDescription Updating a notice by the Admin
     * @apiParam {String} noticeId (Required)
     * @apiSuccessExample {json} Response
     {
  "error": false,
  "data": {
    "_id": "578733748cba62ec0c49f8bc",
    "subject": "jiiiiiiiiiiiii111111UUw",
    "content": "kjdshkjskjssjdlsdla",
    "societyId": "57860082a4d591101e2d0604",
    "createdBy": "578529ffa3b34e8c17dce78e",
    "__v": 0,
    "dateCreated": "2016-07-14T06:38:44.702Z",
    "reminderDate": "2016-07-14T06:38:44.702Z"
  }
}
   */

  apiRoutes.put('/editnotice/:id', auth('Admin'), function (req, res) {
    var id = req.params.id;
    Notice.findByIdAndUpdate(id, req.body, {
      new: true
    }, function (err, notice) {
      if (err) {
        console.log(err);
        return res.json({
          error: true,
          message: "Notice could not be updated"
        });
      } else {
        res.json({
          error: false,
          data: notice
        });
      }
    });
  });
  /**
     * @api {delete} /deletenotice/:noticeId Delete a notice
     * @apiVersion 1.0.0
     * @apiName deleteNotice
     * @apiGroup Notice
     * @apiPermission Admin
     *
     * @apiDescription Updating a notice by the Admin
     * @apiParam {String} noticeId (Required)
     * @apiSuccessExample {json} Response
     {
  "error": false,
  "data": "Notice successfully deleted"
}
     */
  apiRoutes.delete('/deletenotice/:id', auth('Admin'), function (req, res) {
    var noticeId = req.params.id;
    Notice.findByIdAndRemove(noticeId, function (err) {
      if (err) {
        console.log(err);
        return res.json({
          error: true,
          message: "Notice not deleted"
        });
      } else {
        res.json({
          error: false,
          data: "Notice successfully deleted"
        });
      }
    });
  });

  /**
     * @api {post} /addemergencycontact Create a new Emergency Contact
     * @apiVersion 1.0.0
     * @apiName addEmergencyContact
     * @apiGroup Emergency Contact
     * @apiPermission Admin
     *
     * @apiDescription Add a new Emergency Contact with all the necessary details by the  admin
     * @apiParam {String}   type    Type of the emergency contact (Required)
       @apiParam {Array(contacts)}  contacts Array of contacts
       @apiParam {String} contacts.organization Name of the org (Required)
       @apiParam {Number} contatcs.contactPhone Number of the org (Required)
       @apiParam {String} societyId (Required)
       @apiParam {String} createdBy (Required)
       @apiParamExample {json} Request Example
       {
    "type": "Police",

    "contacts":[{
        "organization": "Staton2",
        "contactPhone": 78978323244
    }, {
        "organization": "Staton3",
        "contactPhone": 7897897
    }, {
        "organization": "Staton4",
        "contactPhone": 7897897
    }, {
        "organization": "Staton5",
        "contactPhone": 7897897
    }],

    "societyId": "5785e7b5b7585d9426694023",

    "createdBy": "578529ffa3b34e8c17dce78e"
}

       @apiSuccess {String} id Emergnecy contact Id
    */

  apiRoutes.post('/addemergencycontact', auth('Admin'), function (req, res) {
    var emergencycontact = new EmergencyContact(req.body);
    emergencycontact.save(function (err) {
      if (err) {
        console.log(err);
        return res.json({
          error: true,
          message: "Fill the mandatory fields"
        });
      } else {
        res.json({
          error: false,
          data: {
            id: emergencycontact.id
          }
        });
      }
    });
  });



  /**
       * @api {get} /getemergencycontacts/:societyId Get Emergency Contacts of a society
       * @apiVersion 1.0.0
       * @apiName getEmergencyContacts
       * @apiGroup Emergency Contact
       * @apiPermission Admin, Resident
       *
       * @apiDescription Get all the Emergency Contacts of a particular society
       * @apiParam {String} societyId (Required)

       * @apiSuccessExample {json} Success Response
          {
    "error": false,
    "data": [
      {
        "_id": "5787433d5c6c78040e86c997",
        "type": "Police",
        "societyId": "5785e7b5b7585d9426694023",
        "createdBy": "578529ffa3b34e8c17dce78e",
        "__v": 0,
        "contacts": [
          {
            "organization": "Staton2",
            "contactPhone": 78978323244,
            "_id": "5787433d5c6c78040e86c99b"
          },
          {
            "organization": "Staton3",
            "contactPhone": 7897897,
            "_id": "5787433d5c6c78040e86c99a"
          }
        ]
      },
      {
        "_id": "5787603631aea8b02023a404",
        "type": "Ambulance",
        "societyId": "5785e7b5b7585d9426694023",
        "createdBy": "578529ffa3b34e8c17dce78e",
        "__v": 0,
        "contacts": [
          {
            "organization": "Staton2",
            "contactPhone": 78978323244,
            "_id": "5787603631aea8b02023a408"
          }
        ]
      }
    ]
  }

  */
  apiRoutes.get('/getemergencycontacts/:societyId', auth(['Admin', 'Resident']), function (req, res) {
    var societyId = req.params.societyId;

    EmergencyContact.find({
      "societyId": societyId
    }, function (err, contacts) {
      if (err) {
        console.log(err);
        return res.json({
          error: true,
          message: "No contacts found"
        });
      } else {
        res.json({
          error: false,
          data: contacts
        });

      }
    });
  });


  /**
     * @api {put} /editemergencycontact/:emId Update a Emergency Contact
     * @apiVersion 1.0.0
     * @apiName editEmergencyContact
     * @apiGroup Emergency Contact
     * @apiPermission Admin
     *
     * @apiDescription Updating a Emergency Contact by the Admin. Contacts are overridden. Each request should
        contain all the contacts object
     * @apiParam {String} emergencyContactId Through Url (Required)
       @apiParam {String}   type    Type of the emergency contact
       @apiParam {Array(contacts)}  contacts Array of contacts
       @apiParam {String} contacts.organization Name of the org (Required)
       @apiParam {Number} contatcs.contactPhone Number of the org (Required)
       @apiParam {String} societyId
       @apiParam {String} createdBy
     * @apiSuccessExample {json} Response
     {
  "error": false,
  "data": {
    "_id": "5787433d5c6c78040e86c997",
    "type": "Police",
    "societyId": "5785e7b5b7585d9426694023",
    "createdBy": "578529ffa3b34e8c17dce78e",
    "__v": 0,
    "contacts": [
      {
        "organization": "Jadavpur",
        "contactPhone": 8979889694,
        "_id": "5787668b8d7320d82b918b09"
      },
      {
        "organization": "Anandapur",
        "contactPhone": 8979889694,
        "_id": "5787668b8d7320d82b918b08"
      }
    ]
  }
}
    */

  apiRoutes.put('/editemergencycontact/:emId', auth('Admin'), function (req, res) {
    var id = req.params.emId;
    EmergencyContact.findByIdAndUpdate(id, req.body, {
      new: true
    }, function (err, contact) {
      if (err) {
        console.log(err);
        return res.json({
          error: true,
          message: "Contact could not be updated"
        });
      } else {
        res.json({
          error: false,
          data: contact
        });
      }
    });
  });

  /**
     * @api {delete} /deleteemergencycontact/:emId Delete a Emergency Contact
     * @apiVersion 1.0.0
     * @apiName deleteEmergencyContact
     * @apiGroup Emergency Contact
     * @apiPermission Admin
     *
     * @apiDescription Deleting a Emergency Contact by the Admin
     * @apiParam {String} emeregencyContactId (Required)
     * @apiSuccessExample {json} Response
     {
  "error": false,
  "data": "Emergency Contact successfully deleted"
}*/

  apiRoutes.delete('/deleteemergencycontact/:emId', auth('Admin'), function (req, res) {
    var emId = req.params.emId;
    EmergencyContact.findByIdAndRemove(emId, function (err) {
      if (err) {
        console.log(err);
        return res.json({
          error: true,
          message: "Emergency Contact not deleted"
        });
      } else {
        res.json({
          error: false,
          message: "Emergency Contact successfully deleted"
        });
      }
    });
  });

  /**
     * @api {post} /addboardmember Create one or multiple board members at a time
     * @apiVersion 1.0.0
     * @apiName addBoardMember
     * @apiGroup Board Member
     * @apiPermission Admin
     * @apiDescription It supports batch insert. To create one boardmember send one object.
      To create multiple boardmembers send Array of objects.
      @apiParam {String} name
      @apiParam {String} position
      @apiParam {String} [profilePicURL]
      @apiParam {String} [profilePicKey]
      @apiParam {String} contactEmail
      @apiParam {String} contactPhone
      @apiParam {String} societyId
      @apiParam {String} createdBy Admin Id
      @apiParamExample {json} Request Example
  [{
 "name": "Acharya",
 "societyId": "5785d1c521e3f068239e1d5d",
 "createdBy": "578529cea3b34e8c17dce78d",
 "contactEmail": "i.am.sandeep@gmail.com",
 "contactPhone": 89765454
},{
 "name": "jkasdhkdsj",

 "societyId": "5785d1c521e3f068239e1d5d",
 "createdBy": "578529cea3b34e8c17dce78d",
 "contactEmail": "i.am.sandeep@gmail.com",
 "contactPhone": 89765454
}
}]

@apiSuccess {String} boardMemberId Id of the newly created board members




     **/


  apiRoutes.post('/addboardmember', function (req, res) {
    if (req.body instanceof Array) {
      // if Bulk adding
      BoardMember.insertMany(req.body, function (err, doc) {
        if (err) {
          console.log(err);
          return res.json({
            error: true,
            message: "Bulk Adding Failed"
          });
        } else {


          return res.json({
            error: false,
            data: doc
          });
        }
      });
    } else {
      // if a single object
      var member = new BoardMember(req.body);
      member.save(function (err) {
        if (err) {
          console.log(err);
          return res.json({
            error: true,
            message: "Fill the mandatory fields"
          });
        } else {
          res.json({
            error: false,
            data: {
              id: member.id
            }
          });
        }
      });
    }
  });

  /**
       * @api {get} /getboardmembers/:societyId Get all the board memebers for a society
       * @apiVersion 1.0.0
       * @apiName getBoardMembers
       * @apiGroup Board Member
       * @apiPermission Admin, Resident
        @apiParam {String} societyId
        @apiSuccess {String} name
        @apiSuccess {String} position
        @apiSuccess {String} profilePicURL
        @apiSuccess {String} profilePicKey
        @apiSuccess {String} contactEmail
        @apiSuccess {String} contactPhone
        @apiSuccessExample {json} Success Response
        {
    "error": false,
    "data": [
      {
        "_id": "5787883b577b421c2e393258",
        "name": "Acharya",
        "societyId": "5785d1c521e3f068239e1d5d",
        "createdBy": "578529cea3b34e8c17dce78d",
        "__v": 0,
        "contactPhone": 11111111111,
        "contactEmail": "11111111111@gmail.com"
      },
      {
        "_id": "57878888789d8df40cedbe38",
        "name": "Acharya",
        "societyId": "5785d1c521e3f068239e1d5d",
        "createdBy": "578529cea3b34e8c17dce78d"
      },
      {
        "_id": "57878888789d8df40cedbe39",
        "name": "jkasdhkdsj",
        "societyId": "5785d1c521e3f068239e1d5d",
        "createdBy": "578529cea3b34e8c17dce78d"
      }]
  }
       */

  apiRoutes.get('/getboardmembers/:societyId', auth(['Admin', 'Resident']), function (req, res) {
    var societyId = req.params.societyId;
    BoardMember.find({
      "societyId": societyId
    }, function (err, member) {
      if (err) {
        console.log(err);
        return res.json({
          error: true,
          message: "Member could not be found"
        });
      } else {
        res.json({
          error: false,
          data: member
        });
      }
    });
  });
  /**
       * @api {put} /editboardmember/:boardMemberID Edit one Board Member
       * @apiVersion 1.0.0
       * @apiName editBoardMember
       * @apiGroup Board Member
       * @apiPermission Admin
         @apiParam {String} boardMemberID
        @apiParam {String} name
        @apiParam {String} position
        @apiParam {String} contactEmail
        @apiParam {String} contactPhone
        @apiParamExample {json} Request Example
        {
        "contactEmail": "11111111111@gmail.com",
        "contactPhone": 11111111111
        }

        @apiSuccessExample {json}  Success Example
        {
    "error": false,
    "data": {
      "_id": "5787883b577b421c2e393258",
      "name": "Acharya",
      "contactPhone": 11111111111,
      "contactEmail": "11111111111@gmail.com"
    }
  }

    */


  apiRoutes.put('/editboardmember/:boardMemberID', auth('Admin'), function (req, res) {
    var boardMemberID = req.params.boardMemberID;
    BoardMember.findByIdAndUpdate(boardMemberID, req.body, {
      "new": true
    }, function (err, member) {
      if (err) {
        console.log(err);
        return res.json({
          error: true,
          message: "Member could not be updated"
        });
      } else {
        res.json({
          error: false,
          data: member
        });
      }
    });
  });

  /**
     * @api {post} /addemployee Create one employee
     * @apiVersion 1.0.0
     * @apiName addemployee
     * @apiGroup Manage Employees
     * @apiPermission Resident
     * @apiDescription Add one employee
      @apiParam {String} name
      @apiParam {String} gender
      @apiParam {Date} dob
      @apiParam {Number} contactPhone
      @apiParam {String} role
      @apiParam {String} regularSchedule
      @apiParam {String} addedBy ResidentId
      @apiParam {String} societyId
      @apiParamExample {json} Request Example
      {
        "name": "sujit",
        "gender": "male",
        "addedBy": "57889a00e0a8c7bc1ebdb60c",
        "societyId": "5785d1c521e3f068239e1d5d",
        "dob": "Fri Jul 15 2016 12:41:30",
        "contactPhone": 789789789,
        "role": "Servent",
        "regularSchedule": "Morning"
      }

      @apiSuccess {String} employeeId Newly created employee ID
      @apiSuccessExample {json} Success Example
      {
  "error": false,
  "data": {
    "id": "578cbd2bff1377cc206e2c37"
  }
}

    */

  apiRoutes.post('/addemployee', auth('Resident'), function (req, res) {
    var employee = new ResidentEmployee(req.body);
    employee.save(function (err) {
      if (err) {
        console.log(err);
        return res.json({
          error: true,
          message: "Fill all the mandatory fileds"
        });
      } else {
        res.json({
          error: false,
          data: {
            id: employee.id
          }
        });
      }
    });
  });
  /**
       * @api {get} /getemployees/:residentId Get employees for a resident
       * @apiVersion 1.0.0
       * @apiName getemployees
       * @apiGroup Manage Employees
       * @apiPermission Resident
       * @apiDescription Get employees for a resident
       * @apiParam {String} residentId
          @apiSuccess {String} employeeId
          @apiSuccess {String} name
          @apiSuccess {String} gender
          @apiSuccess {Date} dob
          @apiSuccess {Number} contactPhone
          @apiSuccess {String} role
          @apiSuccess {String} regularSchedule
          @apiSuccessExample {json} Success Example
          {
    "error": false,
    "data": [
      {
        "employeeId": "1234abcd6789"
        "name": "sujit",
        "gender": "male",
        "dob": "2016-07-15T07:11:30.000Z",
        "contactPhone": 789789789,
        "role": "uuu",
        "regularSchedule": "Morning"
      },
      {
        "employeeId": "4351abcd1234"
        "name": "sujit",
        "gender": "male",
        "dob": "2016-07-15T07:11:30.000Z",
        "contactPhone": 789789789,
        "role": "uuu",
        "regularSchedule": "Morning"
      }
    ]
  }
       */

  apiRoutes.get('/getemployees/:residentId', auth('Resident'), function (req, res) {
    var residentId = req.params.residentId;
    ResidentEmployee.find({
      "addedBy": residentId
    }, function (err, employees) {
      if (err) {
        console.log(err);
        return res.json({
          error: true,
          message: "No employees found"
        });
      } else {
        if (employees.length == 0) {
          return res.json({
            error: true,
            message: "No employees found"
          });
        }
        employees = employees.map(function (obj) {
          rObj = {};
          rObj.employeeId = obj.id;
          rObj.name = obj.name;
          rObj.gender = obj.gender;
          rObj.dob = obj.dob;
          rObj.contactPhone = obj.contactPhone;
          rObj.role = obj.role;
          rObj.regularSchedule = obj.regularSchedule;
          rObj.profilePicURL = obj.awsProfilePicURL;
          return rObj;
        });
        return res.json({
          error: false,
          data: employees
        });
      }

    });
  });

  /**
     * @api {delete} /deleteEmployee/:residentEmpID Delete one employee
     * @apiVersion 1.0.0
     * @apiName deleteEmployee
     * @apiGroup Manage Employees
     * @apiPermission Resident
     * @apiDescription Delete employee for a resident
     * @apiParam {String} residentEmpId
     * @apiSuccessExample {json} Success Response
     {
    "error": false,
    "message": "Employee successfully deleted"
      }

     */

  apiRoutes.delete('/deleteEmployee/:residentEmpID', auth('Resident'), function (req, res) {
    var residentEmpID = req.params.residentEmpID;
    ResidentEmployee.findByIdAndRemove(residentEmpID, function (err) {
      if (err) {
        console.log(err);
        return res.json({
          error: true,
          message: "Employee could not be deleted"
        });
      } else {
        res.json({
          error: false,
          message: "Employee successfully deleted"
        });
      }
    });
  });

  /**
     * @api {post} /addfreqvisitor Add one frequent visitor
     * @apiVersion 1.0.0
     * @apiName addfrequentvisitor
     * @apiGroup Frequent Visitor
     * @apiPermission Resident
     * @apiDescription Add one frequent visitor
          @apiParam {String} name
          @apiParam {Array(Number)} contactPhone
          @apiParam {String} expiryDate
          @apiParam {String} token
          @apiParam {String} societyId
          @apiParam {String} addedBy ResidentId
          @apiParamExample {json} Request Example
          {
    "name": "Sandeep",
    "expiryDate":"Fri Jul 15 2016 18:04:31",
    "contactPhone": "9876543210",

    "token": "fsiuhiusdhiushdfiuohsdfouhdsfiudsifdsjfisdjaoja",
    "societyId": "5785d1c521e3f068239e1d5d",
    "addedBy": "578877021787272424e854f0"
}
@apiSuccess {String} freqVisitorID Id of the newly created Visitor
@apiSuccessExample {json} Success Example
{
  "error": false,
  "data": {
    "id": "578cc87dcc170e3413673c65"
  }
}
     */

  apiRoutes.post('/addfreqvisitor', auth('Resident'), function (req, res) {
    var data = req.body;
    data.addedBy = res.locals.user.id;
    var visitor = new FrequentVisitor(data);
    visitor.save(function (err) {
      if (err) {
        console.log(err);
        return res.json({
          error: true,
          message: "Fill all the details"
        });
      } else {
        res.json({
          error: false,
          data: {
            id: visitor.id
          }
        });
      }
    });
  });
  /**
       * @api {get} /getfreqvisitor/:residentId Get frequent visitors for a resident
       * @apiVersion 1.0.0
       * @apiName getfreqvisitor
       * @apiGroup Frequent Visitor
       * @apiPermission Resident
       * @apiParam {String} residentId
        @apiSuccess {String} visitorId
        @apiSuccess {String} name
        @apiSuccess {Array(Number)} contactPhone
        @apiSuccess {Date} expiryDate
        @apiSuccess {String} token
        @apiSuccessExample {json} Success Example
        {
    "error": false,
    "data": [
      {
        "visitorId": "1234abcd6789"
        "name": "kkkkkkkkkkk",
        "token": "fsiuhiusdhiushdfiuohsdfouhdsfiudsifdsjfisdjaoja",
        "contactPhone": [
          8977897987,
          897987987
        ],
        "expiryDate": "2016-07-15T12:34:31.000Z"
      },
      {
        "visitorId": "6789abcd1234"
        "name": "aaaaaaaaaaaaaaaaaaaaaaaaa",
        "token": "fsiuhiusdhiushdfiuohsdfouhdsfiudsifdsjfisdjaoja",
        "contactPhone": [
          8977897987,
          897987987
        ],
        "expiryDate": "2016-07-15T12:34:31.000Z"
      }
    ]
  }
       */

  apiRoutes.get('/getfreqvisitor/:residentId', auth('Resident'), function (req, res) {
    var residentId = req.params.residentId;
    FrequentVisitor.find({
      "addedBy": residentId
    }, function (err, visitors) {
      if (err) {
        console.log(err);
        return res.json({
          error: true,
          message: "Nothing Found"
        });
      } else {
        if (visitors.length == 0) {
          return res.json({
            error: true,
            message: "Nothing Found"
          });
        }
        visitors = visitors.map(function (visitor) {
          var rObj = {};
          rObj.visitorId = visitor.id;
          rObj.name = visitor.name;
          rObj.token = visitor.token;
          rObj.contactPhone = visitor.contactPhone;
          rObj.expiryDate = visitor.expiryDate;
          return rObj;
        });
        res.json({
          error: false,
          data: visitors
        });
      }
    });
  });

  /**
       * @api {delete} /deleteFreqVisitor/:freqVisitorId Delete one frequent visitor
       * @apiVersion 1.0.0
       * @apiName deleteFreqVisitor
       * @apiGroup Frequent Visitor
       * @apiPermission Resident
       * @apiParam {String} freqVisitorId
       @apiSuccessExample {json} Success Example
       {
    "error": false,
    "message": "Successfully deleted"
  }

       */
  apiRoutes.delete('/deleteFreqVisitor/:freqVisitorId', auth('Resident'), function (req, res) {
    var freqVisitorId = req.params.freqVisitorId;
    FrequentVisitor.findByIdAndRemove(freqVisitorId, function (err) {
      if (err) {
        console.log(err);
        return res.json({
          error: true,
          message: "Frequent Visitor could not be deleted"
        });
      } else {
        res.json({
          error: false,
          message: "Successfully deleted"
        });
      }
    });
  });

  /**
     * @api {post} /addOpinionPoll Add Opinion Poll
     * @apiVersion 1.0.0
     * @apiName addOpinionPoll
     * @apiGroup Poll
     * @apiPermission Admin
      @apiParam {String} question
      @apiParam {String} pollCloseDate
      @apiParam {Array(String)} options
      @apiParam {String} societyId
      @apiParam {String} addedBy AdminId
      @apiParamExample {json} Requestt Example
{
    "question": "What is real name?",
    "options": [
    "yuifdjhfdkhdj",
    "hhhhhhhhhhh",
    "aaaaaaaaaa",
    "jjjjjjjjjjjj",
    "yyyyyyyyyyyy"
    ],
    "societyId": "5785d1c521e3f068239e1d5d",
    "addedBy": "578529cea3b34e8c17dce78d"
}

  @apiSuccess {String} pollId Newly created poll Id
     */

  apiRoutes.post('/addOpinionPoll', auth('Resident'), function (req, res) {
    var opinionpoll = new OpinionPoll(req.body);
    opinionpoll.save(function (err) {
      if (err) {
        console.log(err);
        return res.json({
          error: true,
          message: "Poll could not be added"
        });
      } else {
        res.json({
          error: false,
          data: {
            id: opinionpoll.id
          }
        });
      }
    });
  });


  /**
     * @api {put} /respondToOpinionPoll/:pollId Respond to a poll
     * @apiVersion 1.0.0
     * @apiName respondToOpinionPoll
     * @apiGroup Poll
     * @apiPermission Resident
     @apiDescription Residents are allowed to submit a response for a poll only once
     @apiParam {String} pollId Through URl
      @apiParam {Number} optionSelected
      @apiParam {String} responsedBy Resident Id
      @apiParamExample {json} Request Example
      {
    "optionSelected": 3,
    "responsedBy": "57889a00e0a8c7bc1ebdb60c"
}
@apiSuccessExample {json} Success Response
{
  "error": true,
  "message": "Already Submitted the response"
}
     */

  apiRoutes.put('/respondToOpinionPoll/:pollId', auth('Resident'), function (req, res) {
    var pollId = req.params.pollId;
    OpinionPoll.findDuplicateResponse(pollId, req.body.responsedBy, function (err, doc) {
      if (err) {
        return res.json({
          error: true,
          message: "Some Internal error occured"
        });
      } else {
        if (doc.length > 0) {
          return res.json({
            error: true,
            message: "Already Submitted the response"
          });
        } else {
          OpinionPoll.findByIdAndUpdate(pollId, {
            $push: {
              "responses": req.body
            }
          }, {
            new: true
          }, function (err, poll) {
            if (err) {
              return res.json({
                error: true,
                message: "Could not be saved"
              });
            } else {
              res.json({
                error: false,
                message: "Successfully saved"
              });
            }
          });
        }
      }
    });
  });

  /**
     * @api {get} /getOpinionPollResponses/:pollId Get responses for a poll
     * @apiVersion 1.0.0
     * @apiName getOpinionPollResponses
     * @apiGroup Poll
     * @apiPermission Admin, Resident
     * @apiParam {String} pollId
      @apiSuccess {String} question
      @apiSuccess {Date} pollCreatedDate
      @apiSuccess {Date} pollCloseDate
      @apiSuccess {Array(String)} options
      @apiSuccess {Array(Objects)} responses
      @apiSuccess {String} responses.optionSelected
      @apiSuccess {String} responses.responseTime
      @apiSuccess {String} responses.responsedBy
      @apiSuccess {String} societyId
      @apiSuccess {String} addedBy Resident Id
      @apiSuccessExample {json} Success Example
      {
  "error": false,
  "data": {
    "_id": "578c9f9909609464132b3acb",
    "question": "Hi.......What is real name?",
    "societyId": "5785d1c521e3f068239e1d5d",
    "addedBy": "578529cea3b34e8c17dce78d",
    "responses": [
      {
        "responsedBy": "57889a00e0a8c7bc1ebdb60c",
        "optionSelected": 3,
        "_id": "578c9fa509609464132b3acc",
        "responseTime": "2016-07-18T09:21:41.334Z"
      },
      {
        "responsedBy": "57889a00e0a8c7bc1ebdb60d",
        "optionSelected": 3,
        "_id": "578cb4e480276c0c103cee89",
        "responseTime": "2016-07-18T10:52:20.282Z"
      }
    ],
    "options": [
      "yuifdjhfdkhdj",
      "hhhhhhhhhhh",
      "aaaaaaaaaa",
      "jjjjjjjjjjjj",
      "yyyyyyyyyyyy"
    ],
    "pollCreatedDate": "2016-07-18T09:21:29.088Z"
  }
}
     */


  apiRoutes.get('/getOpinionPollResponses/:pollId', auth(['Admin', 'Resident']), function (req, res) {
    var pollId = req.params.pollId;
    OpinionPoll.findById(pollId, function (err, poll) {
      if (err) {
        console.log(err);
        return re.json({
          error: true,
          message: "Poll could not be found"
        });
      } else {
        return res.json({
          error: false,
          data: poll
        });
      }
    });
  });


  /**
     * @api {put} /editOpinionPoll/:pollId Edit opinion poll
     * @apiVersion 1.0.0
     * @apiName editOpinionPoll
     * @apiGroup Poll
     * @apiPermission Admin
     * @apiParam {String} pollId
     * @apiParam {String} question
     * @apiParam {Array(String)} options
      @apiParamExample {json} Request Example
      {
      "question": "Hi.......What is your real name?"
      }
      @apiSuccess {String} question
      @apiSuccess {Date} pollCreatedDate
      @apiSuccess {Date} pollCloseDate
      @apiSuccess {Array(String)} options
      @apiSuccess {Array(Objects)} responses
      @apiSuccess {String} responses.optionSelected
      @apiSuccess {String} responses.responseTime
      @apiSuccess {String} responses.responsedBy
      @apiSuccess {String} societyId
      @apiSuccess {String} addedBy Resident Id
      @apiSuccessExample {json} Success Example
      {
  "error": false,
  "data": {
    "_id": "578c9f9909609464132b3acb",
    "question": "Hi.......What is your real name?",
    "societyId": "5785d1c521e3f068239e1d5d",
    "addedBy": "578529cea3b34e8c17dce78d",
    "responses": [
      {
        "responsedBy": "57889a00e0a8c7bc1ebdb60c",
        "optionSelected": 3,
        "_id": "578c9fa509609464132b3acc",
        "responseTime": "2016-07-18T09:21:41.334Z"
      },
      {
        "responsedBy": "57889a00e0a8c7bc1ebdb60d",
        "optionSelected": 3,
        "_id": "578cb4e480276c0c103cee89",
        "responseTime": "2016-07-18T10:52:20.282Z"
      }
    ],
    "options": [
      "yuifdjhfdkhdj",
      "hhhhhhhhhhh",
      "aaaaaaaaaa",
      "jjjjjjjjjjjj",
      "yyyyyyyyyyyy"
    ],
    "pollCreatedDate": "2016-07-18T09:21:29.088Z"
  }
}
     */
  apiRoutes.put('/editOpinionPoll/:pollId', auth('Admin'), function (req, res) {
    var pollId = req.params.pollId;
    OpinionPoll.findByIdAndUpdate(pollId, req.body, {
      new: true
    }, function (err, poll) {
      if (err) {
        console.log(err);
        return re.json({
          error: true,
          message: "Poll could not be found"
        });
      } else {
        res.json({
          error: false,
          data: poll
        });
      }
    });
  });


  /**
       * @api {get} /getOpinionPolls/:societyId Get all opinion polls for a society
       * @apiVersion 1.0.0
       * @apiName getOpinionPolls
       * @apiGroup Poll
       * @apiPermission Admin, Resident

       * @apiParam {String} societyId Note: You've to mention this in the URL itself [Mandatory]

       @apiSuccess {Array(Object)} data
       @apiSuccess {String} data.question
        @apiSuccess {Date} data.pollCreatedDate
        @apiSuccess {Date} data.pollCloseDate
        @apiSuccess {Array(String)} data.options
        @apiSuccess {Array(Objects)} data.responses
        @apiSuccess {String} data.responses.optionSelected
        @apiSuccess {String} data.responses.responseTime
        @apiSuccess {String} data.responses.responsedBy
        @apiSuccess {String} data.societyId
        @apiSuccess {String} data.addedBy Resident Id
        @apiSuccessExample {json} Success Example
        {
    "error": false,
    "data": [
      {
        "_id": "578c9f9909609464132b3acb",
        "question": "Hi.......What is real name?",
        "societyId": "5785d1c521e3f068239e1d5d",
        "addedBy": "578529cea3b34e8c17dce78d",
        "__v": 0,
        "responses": [
          {
            "responsedBy": "57889a00e0a8c7bc1ebdb60c",
            "optionSelected": 3,
            "_id": "578c9fa509609464132b3acc",
            "responseTime": "2016-07-18T09:21:41.334Z"
          },
          {
            "responsedBy": "57889a00e0a8c7bc1ebdb60d",
            "optionSelected": 3,
            "_id": "578cb4e480276c0c103cee89",
            "responseTime": "2016-07-18T10:52:20.282Z"
          }
        ],
        "options": [
          "yuifdjhfdkhdj",
          "hhhhhhhhhhh",
          "aaaaaaaaaa",
          "jjjjjjjjjjjj",
          "yyyyyyyyyyyy"
        ],
        "pollCreatedDate": "2016-07-18T09:21:29.088Z"
      },
      {
        "_id": "578cb54d80276c0c103cee8a",
        "question": "What is real name?",
        "societyId": "5785d1c521e3f068239e1d5d",
        "addedBy": "578529cea3b34e8c17dce78d",
        "__v": 0,
        "responses": [
          {
            "responsedBy": "57889a00e0a8c7bc1ebdb60c",
            "optionSelected": 3,
            "_id": "578cb55480276c0c103cee8b",
            "responseTime": "2016-07-18T10:54:12.808Z"
          }
        ],
        "options": [
          "yuifdjhfdkhdj",
          "hhhhhhhhhhh",
          "aaaaaaaaaa",
          "jjjjjjjjjjjj",
          "yyyyyyyyyyyy"
        ],
        "pollCreatedDate": "2016-07-18T10:54:05.895Z"
      },
      {
        "_id": "578ccb773922b40819dd33da",
        "question": "What is real name?",
        "societyId": "5785d1c521e3f068239e1d5d",
        "addedBy": "578529cea3b34e8c17dce78d",
        "__v": 0,
        "responses": [],
        "options": [
          "yuifdjhfdkhdj",
          "hhhhhhhhhhh",
          "aaaaaaaaaa",
          "jjjjjjjjjjjj",
          "yyyyyyyyyyyy"
        ],
        "pollCreatedDate": "2016-07-18T12:28:39.699Z"
      }
    ]
  }

       */

  apiRoutes.get('/getOpinionPolls/:societyId', auth(['Admin','Resident']), function (req, res) {
    OpinionPoll.find({societyId: req.params.societyId}, function (err, polls) {
      if (err) {
        console.log(err);
        return re.json({
          error: true,
          message: "Polls could not be found"
        });
      } else {
        res.json({
          error: false,
          data: polls
        });
      }
    });
  });

  /** @api {post} /addResidentDoc Add a new Resident document
     * @apiVersion 1.0.0
     * @apiName addResidentDoc
     * @apiGroup Resident document
     * @apiPermission Resident
     *@apiParam {String} docType
      @apiParam {String} note
      @apiParam {String} userDocKey
      @apiParam {String} userDocURL
      @apiParam {String} societyId
      @apiParam {String} addedBy Resident Id
      @apiParam {Object} [aws] As obtained after uploading the actual file to AWS using /uploaddocument
      @apiParam {String} [location] As obtained after uploading the actual file to AWS using /uploaddocument
      @apiParamExample {json} Request Example
      {
    "docType": "kkkkkkkk",
    "note": "jfkdhjkdfhkdjfs",
    "societyId": "5785d1c521e3f068239e1d5d",
    "addedBy": "578877021787272424e854f0"
      }
      @apiSuccess {String} docId Newly created doc Id
*/

  apiRoutes.post('/addResidentDoc', auth('Resident'), function (req, res) {
    var doc = new ResidentDocument(req.body);
    doc.save(function (err) {
      if (err) {
        console.log(err);
        return res.json({
          error: true,
          message: "Error in uploading the doc"
        });
      } else {
        res.json({
          error: false,
          data: {
            id: doc._id
          }
        });
      }
    });
  });

  /**
     * @api {get} /getResidentDocs/:societyId Get resident Documents
     * @apiVersion 1.0.0
     * @apiName getResidentDocs
     * @apiGroup Resident document
     * @apiPermission Resident
      @apiSuccessExample {json} Success Response
      {
    "error": false,
    "data": [
      {
        "_id": "57c6906f68b81f4c24220154",
        "docType": "AADHAR",
        "note": "test",
        "societyId": "57b6d30246d40638a6d4c16b",
        "addedBy": {
          "_id": "579b13562525d146251a4d6f",
          "name": "Sandeep Acharya",
          "awsProfilePicURL": "https://s3-ap-southeast-1.amazonaws.com/sayanriju-smart-images/579b13562525d146251a4d6f",
          "id": "579b13562525d146251a4d6f"
        },
        "__v": 0,
        "aws": {
          "key": "57c6905b68b81f4c24220153",
          "bucket": "sayanriju-smart",
          "fileName": "2016-08-11-12-02-19-178_1470897139178_XXXPC1386X_Acknowledgement.pdf"
        },
        "uploadDateTime": "2016-08-31T08:08:15.323Z"
      },
      {
        "_id": "58070fcbdbe6e77f268f9d25",
        "docType": "AADHAR",
        "note": "TEST",
        "societyId": "57b6d30246d40638a6d4c16b",
        "addedBy": {
          "_id": "58070f3edbe6e77f268f9d23",
          "name": "Test Resident",
          "awsProfilePicURL": "https://s3-ap-southeast-1.amazonaws.com/sayanriju-smart-images/58070f3edbe6e77f268f9d23",
          "id": "58070f3edbe6e77f268f9d23"
        },
        "__v": 0,
        "aws": {
          "key": "58070fb7dbe6e77f268f9d24",
          "bucket": "sayanriju-smart",
          "fileName": "airtel_mukherjee.pdf"
        },
        "uploadDateTime": "2016-10-19T06:16:43.915Z"
      }
    ]
  }
     */

  apiRoutes.get('/getResidentDocs/:societyId', auth('Resident'), function (req, res) {
    var societyId = req.params.societyId;
    ResidentDocument.find({
      "societyId": societyId
    })
    .populate("addedBy", "name")
    .exec(function (err, doc) {
      if (err) {
        return res.json({
          error: true,
          message: "Error finding the documents"
        });
      } else {
        res.json({
          error: false,
          data: doc
        });
      }
    });
  });

  /**
       * @api {delete} /deleteResidentDoc/:docId Delete resident Documents
       * @apiVersion 1.0.0
       * @apiName deleteResidentDoc
       * @apiGroup Resident document
       * @apiPermission Resident
       @apiParam {String} docId
      @apiSuccessExample {json} Success Response
      {
        error: false,
        message: "Successfully deleted"
      }

       */


  apiRoutes.delete('/deleteResidentDoc/:docId', auth('Resident'), function (req, res) {
    var docId = req.params.docId;
    ResidentDocument.findByIdAndRemove(docId, function (err) {
      if (err) {
        console.log(err);
        return res.json({
          error: true,
          message: "Error in deleting the document"
        });
      } else {
        res.json({
          error: false,
          message: "Successfully deleted"
        });
      }
    });
  });

  /**
    * @api {post} /addClassifiedAd Add new classified Ad
     * @apiVersion 1.0.0
     * @apiName addClassifiedAd
     * @apiGroup Classified Ad
     * @apiPermission Resident
      @apiParam {Boolean} isOffering
      @apiParam {String} for
      @apiParam {String} category
      @apiParam {String} title
      @apiParam {Number} price only if isOffering true
      @apiParam {String} [postAs] Nickname
      @apiParam {String} contactEmail
      @apiParam {Date} from
      @apiParam {Date} validTill
      @apiParam {Array(String)} pictureURLs
      @apiParam {String} description
      @apiParam {String} societyId
      @apiParam {String} addedBy
      @apiParamExample {json} Request Example
      {
    "isOffering": true,
    "title": "Hellsdssdo",
    "price": 7897997,
    "pictureURLs": ["jksadhkjsda", "jdshjksdhksjah"],
    "societyId": "5785d1c521e3f068239e1d5d",
    "addedBy": "578877021787272424e854f0"
}
@apiSuccess {String} adId newly created ad id
    */


  apiRoutes.post('/addClassifiedAd', auth('Resident'), function (req, res) {
    var ad = new Classified(req.body);
    ad.save(function (err) {
      if (err) {
        console.log(err);
        return res.json({
          error: true,
          message: "Could not be saved"
        });
      } else {
        res.json({
          error: false,
          data: {
            id: ad.id
          }
        });
      }
    });
  });

  /**
    * @api {get} /getClassifiedAds/:residentId Get all the ads posted by a resident
    * @apiVersion 1.0.0
     * @apiName getClassifiedAds
     * @apiGroup Classified Ad
     * @apiPermission Resident
     * @apiParam {String} residentId
      @apiSuccess {Boolean} isOffering
      @apiSuccess {String} for
      @apiSuccess {String} category
      @apiSuccess {String} title
      @apiSuccess {Number} price only if isOffering true
      @apiSuccess {String} [postAs] Nickname
      @apiSuccess {String} contactEmail
      @apiSuccess {Date} from
      @apiSuccess {Date} validTill
      @apiSuccess {Array(String)} pictureURLs
      @apiSuccess {String} description
      @apiSuccess {String} societyId
      @apiSuccess {String} addedBy
      @apiSuccessExample {json} Success Example
      {
  "error": false,
  "data": [
    {
      "_id": "578dcc832bbffc5000de0099",
      "isOffering": true,
      "title": "Hellsdssdo",
      "price": 7897997,
      "societyId": "5785d1c521e3f068239e1d5d",
      "addedBy": "578877021787272424e854f0",
      "__v": 0,
      "pictureURLs": [
        "jksadhkjsda",
        "jdshjksdhksjah"
      ],
      "from": "2016-07-19T06:45:23.682Z"
    }
  ]
}

*/

  apiRoutes.get('/getClassifiedAds/:residentId', auth('Resident'), function (req, res) {
    var residentId = req.params.residentId;
    Classified.find({
      "addedBy": residentId
    }, function (err, ad) {
      if (err) {
        console.log(err);
        return res.json({
          error: true,
          message: "Could not get anything"
        });
      } else {
        res.json({
          error: false,
          data: ad
        });
      }
    });
  });

  /**
    * @api {get} /getSocietyClassifiedAds/:societyId Get ALL the ads for a society
    * @apiVersion 1.0.0
     * @apiName getSocietyClassifiedAds
     * @apiGroup Classified Ad
     * @apiPermission Resident, Admin
     *  @apiDescription Get ALL the ads for a society
     * @apiParam {String} societyId Note: You've to mention this in the URL itself [Mandatory]
      @apiSuccess {Boolean} isOffering
      @apiSuccess {String} for
      @apiSuccess {String} category
      @apiSuccess {String} title
      @apiSuccess {Number} price only if isOffering true
      @apiSuccess {String} [postAs] Nickname
      @apiSuccess {String} contactEmail
      @apiSuccess {Date} from
      @apiSuccess {Date} validTill
      @apiSuccess {Array(String)} pictureURLs
      @apiSuccess {String} description
      @apiSuccess {String} societyId
      @apiSuccess {String} addedBy
      @apiSuccessExample {json} Success Example
      {
  "error": false,
  "data": [
    {
      "_id": "578dcc832bbffc5000de0099",
      "isOffering": true,
      "title": "Hellsdssdo",
      "price": 7897997,
      "societyId": "5785d1c521e3f068239e1d5d",
      "addedBy": "578877021787272424e854f0",
      "__v": 0,
      "pictureURLs": [
        "jksadhkjsda",
        "jdshjksdhksjah"
      ],
      "from": "2016-07-19T06:45:23.682Z"
    }
  ]
}

*/

  apiRoutes.get('/getSocietyClassifiedAds/:societyId', auth('Resident', 'Admin'), function (req, res) {
    Classified.find({
      "societyId": req.params.societyId
    }, function (err, ad) {
      if (err) {
        console.log(err);
        return res.json({
          error: true,
          message: "Could not get anything"
        });
      } else {
        res.json({
          error: false,
          data: ad
        });
      }
    });
  });


  /**
      * @api {put} /editClassifedAd/:adId Edit a classified Ad
      * @apiVersion 1.0.0
       * @apiName editClassifedAd
       * @apiGroup Classified Ad
       * @apiPermission Resident
       * @apiParam {String} adId
        @apiParam {String} for
        @apiParam {String} category
        @apiParam {String} title
        @apiParam {Number} [price] only if isOffering true
        @apiParam {String} [postAs] Nickname
        @apiParam {String} contactEmail
        @apiParam {Date} from
        @apiParam {Date} validTill
        @apiParam {Array(String)} pictureURLs
        @apiParam {String} description
        @apiParamExample  {json} Request Example
        {
        "pictureURLs": [
          "jksadhkjsda",
          "jdshjksdhksjah",
          "654564654654"
        ]
      }

    @apiSuccessExample {json} Success Response
      {
    "error": false,
    "data": {
      "_id": "578dcc5e2bbffc5000de0098",
      "isOffering": true,
      "title": "Hello",
      "price": 7897997,
      "societyId": "5785d1c521e3f068239e1d5d",
      "addedBy": "578877021787272424e854f0",
      "__v": 0,
      "pictureURLs": [
        "jksadhkjsda",
        "jdshjksdhksjah",
        "654564654654"
      ],
      "from": "2016-07-19T06:44:46.530Z"
    }
  }

       */
  apiRoutes.put('/editClassifedAd/:adId', auth('Resident'), function (req, res) {
    var adId = req.params.adId;
    Classified.findByIdAndUpdate(adId, req.body, {
      new: true
    }, function (err, ad) {
      if (err) {
        console.log(err);
        return res.json({
          error: true,
          message: "Could not be updated"
        });
      } else {
        res.json({
          error: false,
          data: ad
        });
      }
    });
  });

  /**
     * @api {delete} /deleteClassifedAd/:adId Delete resident ad
     * @apiVersion 1.0.0
     * @apiName deleteResidentDoc
     * @apiGroup Resident document
     * @apiPermission Resident
     @apiParam {String} adId
    @apiSuccessExample {json} Success Response
    {
      error: false,
      message: "Successfully deleted"
    }

     */

  apiRoutes.delete('/deleteClassifedAd/:adId', auth('Resident'), function (req, res) {
    var adId = req.params.adId;
    Classified.findByIdAndRemove(adId, function (err) {
      if (err) {
        console.log(err);
        return res.json({
          error: true,
          message: "Error in deleting the ad"
        });
      } else {
        res.json({
          error: false,
          message: "Successfully deleted"
        });
      }
    });
  });


  /**
      * @api {post} /createContact Create A Contact
      * @apiVersion 1.0.0
       * @apiName createContact
       * @apiGroup Contact Us
       * @apiPermission Resident
        @apiParam {String} openedBy
        @apiParam {String} societyId
        @apiParam {String} subject
        @apiParam {String} [category=General] 'General' | 'Complaints' | 'Suggestion'
        @apiParam {String} [status="In Progress"] 'In Progress' | 'Closed'
        @apiParam {Date} [dateOpened=Date.now()]
  */

  apiRoutes.post('/createContact', auth('Resident'), function (req, res) {
    var contact = new Contact(req.body);

    contact.save(function (err) {
      if (err) {
        console.log(err);
        return res.json({
          error: true,
          message: "Oops! Some error occured",
          reason: err
        });
      } else {
        res.json({
          error: false,
          data: {
            id: contact.id
          }
        });
      }
    });
  });

  /**
      * @api {get} /getMyContacts/:societyId/:userId Get Contacts List for User
      * @apiVersion 1.0.0
       * @apiName getMyContacts
       * @apiGroup Contact Us
       * @apiPermission Anyone
        @apiParam {String} societyId
        @apiParam {String} userId
  */

  apiRoutes.get('/getMyContacts/:societyId/:userId', function (req, res) {

    var societyId = req.params.societyId;
    var userId = req.params.userId;

    Contact.find({
      "openedBy": userId,
      "societyId": societyId
    }, function (err, contacts) {
      if (err) {
        console.log(err);
        return res.json({
          error: true,
          message: "Could not find any result"
        });
      } else {
        res.json({
          error: false,
          data: contacts.map(function (contact) {
            var robj = {};
            robj.id = contact.id;
            robj.category = contact.category;
            robj.status = contact.status;
            robj.description = contact.description;
            robj.dateOpened = contact.dateOpened;
            robj.subject = contact.subject;
            robj.openedBy = contact.openedBy;
            return robj;
          })
        });
      }
    });
  });

  /**
      * @api {get} /getContacts Get Filtered list of Contacts
      * @apiVersion 1.0.0
       * @apiName getContacts
       * @apiGroup Contact Us
       * @apiPermission Anyone
        @apiParam {String} societyId
        @apiParam {String} [status] Filter contacts by this status
        @apiParam {String} [category] Filter contacts by this category
        @apiParam {String} [block] Filter contacts by this Block No.
        @apiParam {String} [flatNo] Filter contacts by this Flat No.


  */

  // Most Critical one :)
  apiRoutes.get('/getContacts/', function (req, res) {
    var societyId = req.query.societyId; // must
    var result = [];
    var query;
    if (req.query.status && !req.query.category && !req.query.block && !req.query.flatNo) {
      query = Contact.find({
          "societyId": societyId,
          "status": req.query.status
        })
        .populate({
          path: 'openedBy',
          select: 'name block flatNo'
        });
    } else if (req.query.category && !req.query.status && !req.query.block && !req.query.flatNo) {
      query = Contact.find({
          "societyId": societyId,
          "category": req.query.category
        })
        .populate({
          path: 'openedBy',
          select: 'name block flatNo'
        });
    } else if (!req.query.status && !req.query.category && req.query.block && !req.query.flatNo) {
      query = Contact.find({
          "societyId": societyId
        })
        .populate({
          path: 'openedBy',
          select: 'name block flatNo',
          match: {
            block: req.query.block
          }
        });
    } else if (!req.query.status && !req.query.category && !req.query.block && req.query.flatNo) {
      query = Contact.find({
          "societyId": societyId
        })
        .populate({
          path: 'openedBy',
          select: 'name block flatNo',
          match: {
            flatNo: req.query.flatNo
          }
        })
    } else if (req.query.status && req.query.category && !req.query.block && !req.query.flatNo) {
      query = Contact.find({
          "societyId": societyId,
          "status": req.query.status,
          "category": req.query.category
        })
        .populate({
          path: 'openedBy',
          select: 'name block flatNo'
        });
    } else if (!req.query.status && req.query.category && req.query.block && !req.query.flatNo) {
      query = Contact.find({
          "societyId": societyId,
          "category": req.query.category
        })
        .populate({
          path: 'openedBy',
          select: 'name block flatNo',
          match: {
            block: req.query.block
          }
        });
    } else if (!req.query.status && !req.query.category && req.query.block && req.query.flatNo) {
      query = Contact.find({
          "societyId": societyId
        })
        .populate({
          path: 'openedBy',
          select: 'name block flatNo',
          match: {
            block: req.query.block,
            flatNo: req.query.flatNo
          }
        });
    } else if (req.query.status && !req.query.category && req.query.block && !req.query.flatNo) {
      query = Contact.find({
          "societyId": societyId,
          "status": req.query.status
        })
        .populate({
          path: 'openedBy',
          select: 'name block flatNo',
          match: {
            flatNo: req.query.flatNo
          }
        });
    } else if (req.query.status && !req.query.category && !req.query.block && req.query.flatNo) {
      query = Contact.find({
          "societyId": societyId,
          "status": req.query.status
        })
        .populate({
          path: 'openedBy',
          select: 'name block flatNo',
          match: {
            flatNo: req.query.flatNo
          }
        });
    } else if (!req.query.status && req.query.category && !req.query.block && req.query.flatNo) {
      query = Contact.find({
          "societyId": societyId,
          "category": req.query.category
        })
        .populate({
          path: 'openedBy',
          select: 'name block flatNo',
          match: {
            flatNo: req.query.flatNo
          }
        });
    } else if (!req.query.status && req.query.category && !req.query.block && req.query.flatNo) {
      query = Contact.find({
          "societyId": societyId,
          "category": req.query.category
        })
        .populate({
          path: 'openedBy',
          select: 'name block flatNo',
          match: {
            flatNo: req.query.flatNo
          }
        });
    } else if (req.query.status && req.query.category && req.query.block && !req.query.flatNo) {
      query = Contact.find({
          "societyId": societyId,
          "status": req.query.status,
          "category": req.query.category
        })
        .populate({
          path: 'openedBy',
          select: 'name block flatNo'
        });
    } else if (!req.query.status && req.query.category && req.query.block && req.query.flatNo) {
      query = Contact.find({
          "societyId": societyId,
          "category": req.query.category
        })
        .populate({
          path: 'openedBy',
          select: 'name block flatNo',
          match: {
            block: req.query.block,
            flatNo: req.query.flatNo
          }
        });
    } else if (req.query.block && req.query.flatNo && req.query.status && !req.query.category) {
      query = Contact.find({
          "societyId": societyId,
          "status": req.query.status
        })
        .populate({
          path: 'openedBy',
          select: 'name block flatNo',
          match: {
            block: req.query.block,
            flatNo: req.query.flatNo
          }
        });
    } else if (!req.query.block && req.query.flatNo && req.query.status && req.query.category) {
      query = Contact.find({
          "societyId": societyId,
          "status": req.query.status
        })
        .populate({
          path: 'openedBy',
          select: 'name block flatNo',
          match: {
            flatNo: req.query.flatNo
          }
        });
    } else {
      query = Contact.find({
          "societyId": societyId,
          "status": req.query.status,
          "category": req.query.category
        })
        .populate({
          path: 'openedBy',
          select: 'name block flatNo',
          match: {
            block: req.query.block,
            flatNo: req.query.flatNo
          }
        });
    }
    query.exec(function (err, contacts) {
      if (err) {
        console.log(err);
        return res.json({
          error: true,
          message: "No data found"
        });
      } else {
        contacts.forEach(function (contact) {
          if (contact.openedBy) {
            result.push(contact);
          }
        });
        res.json({
          error: false,
          data: result
        });
      }
    });
  });

  /**
     * @api {put} /changeContactStatus/:contactId Change Contact Status
     * @apiVersion 1.0.0
     * @apiName changeContactStatus
     * @apiGroup Contact Us
     * @apiPermission Anyone
     * @apiDescription Change Contact Status
     * @apiParam {String} contactId
     * @apiParam {String} status="In Progress" 'In Progress' | 'Closed'
     */

  apiRoutes.put('/changeContactStatus/:contactId', function (req, res) {
    var contactId = req.params.contactId;
    Contact.findByIdAndUpdate(contactId, req.body, {
      new: true
    }, function (err, contact) {
      if (err) {
        console.log(err);
        return res.json({
          error: true,
          message: "Could not be updated"
        });
      } else {
        res.json({
          error: false,
          data: {
            id: contact.id
          }
        });
      }
    });
  });


  /**
     * @api {post} /replyToContact Reply to Contact
     * @apiVersion 1.0.0
     * @apiName replyToContact
     * @apiGroup Contact Us
     * @apiPermission Anyone
     * @apiDescription Reply to Contact with a Message
     * @apiParam {String} userId
     * @apiParam {String} contactId
     * @apiParam {String} message
     */

  apiRoutes.post('/replyToContact', function (req, res) {
    var userId = req.body.userId;
    var message = req.body.message;
    var contactId = req.body.contactId;
    Contact.findById(contactId, function (err, contact) {
      if (err) {
        console.log(err);
        return res.json({
          error: true,
          message: "No contacts found with this Id"
        });
      } else {
        contact.messages.push({
          "userId": userId,
          "message": message
        });
        contact.save(function (err) {
          if (err) {
            console.log(err);
            return res.json({
              error: true,
              message: "No contacts found with this Id"
            });
          } else {
            res.json({
              error: false,
              data: contact
            });
          }
        });
      }
    });
  });

/**
   * @api {post} /addService Reply to Contact
   * @apiVersion 1.0.0
   * @apiName addService
   * @apiGroup Services
   * @apiPermission Admin
   * @apiDescription Add a Service
   * @apiParam {String} serviceName
   * @apiParam {String} [description]
   * @apiParam {String} createdBy
   * @apiParam {String} societyId
   */


apiRoutes.post('/addService', auth('Admin'), function (req, res) {
  var serviceoffered = new ServiceOffered(req.body);
  serviceoffered.save(function (err) {
    if (err) {
      console.log(err);
      return res.json({
        error: true,
        message: "Could not be saved",
        reason: err
      });
    } else {
      res.json({
        error: false,
        data: {
          id: serviceoffered.id
        }
      });
    }
  });
});

/**
   * @api {get} /getServices/:societyId Get Services List for a Society
   * @apiVersion 1.0.0
   * @apiName getServices
   * @apiGroup Services
   * @apiPermission Admin, Resident
   * @apiDescription Get Services for Society
   * @apiParam {String} societyId NOTE: You must pass it in the URL itself [MANDATORY]
   */

apiRoutes.get('/getServices/:societyId', auth(['Admin', 'Resident']), function(req, res) {
    var societyId = req.params.societyId;
    ServiceOffered
        .find({societyId: societyId})
        .populate('serviceProvider')
        .exec(function(err, services) {
            if (err) {
                console.log(err);
                return res.json({
                    error: true,
                    message: "Could not get any services",
                    reason: err
                });
            } else {
                res.json({
                    error: false,
                    data: services
                });
            }
        });
});

/**
   * @api {delete} /deleteService/:serviceId Delete a Service
   * @apiVersion 1.0.0
   * @apiName deleteService
   * @apiGroup Services
   * @apiPermission Admin
   * @apiDescription Delete a Service
   * @apiParam {String} serviceId NOTE: You must pass it in the URL itself [MANDATORY]
   */

apiRoutes.delete('/deleteService/:serviceId', auth('Admin'), function (req, res) {
  var serviceId = req.params.serviceId;

  ServiceOffered.findByIdAndRemove(serviceId, function (err) {
    if (err) {
        console.log(err);
        return res.json({
            error: true,
            message: "Could not be deleted",
            reason: err
        });
    } else {
        res.json({
            error: false,
            message: "Successfully deleted"
        });
    }
  });
});

/**
   * @api {put} /editService/:serviceId Edit a Service
   * @apiVersion 1.0.0
   * @apiName editService
   * @apiGroup Services
   * @apiPermission Admin
   * @apiDescription Edit a Service
   * @apiParam {String} serviceId NOTE: You must pass it in the URL itself [MANDATORY]
   */

apiRoutes.put('/editService/:serviceId', auth('Admin'), function (req, res) {
  var serviceId = req.params.serviceId;
  ServiceOffered.findByIdAndUpdate(serviceId, req.body, {new: true}, function (err, services) {
    if (err) {
      console.log(err);
      return res.json({
        error: true,
        message: "Could not be saved",
        reason: err
      });
    } else {
      res.json({
        error: false,
        data: services
      });
    }
  });
});

/**
   * @api {post} /addProvider Add a Service Provider
   * @apiVersion 1.0.0
   * @apiName addProvider
   * @apiGroup Services
   * @apiPermission Admin
   * @apiDescription Add a Service Provider
   * @apiParam {String} name
   * @apiParam {String} contactPhone
   * @apiParam {String} daysWorking
   * @apiParam {String} workHours
   * @apiParam {String} [status='inactive'] Either 'active' or 'inactive'
   * @apiParam {String} createdBy
   * @apiParam {String} societyId
   */

apiRoutes.post('/addProvider', auth('Admin'), function (req, res) {
  var serviceProvider = new ServiceProvider(req.body);
  serviceProvider.save(function (err) {
    if (err) {
      console.log(err);
      return res.json({
        error: true,
        message: "Could not be saved",
        reason: err
      });
    } else {
      res.json({
        error: false,
        data: {
          id: serviceProvider.id
        }
      });
    }
  });
});

/**
   * @api {put} /changeProviderStatus/:providerId Change status of a Service Provider
   * @apiVersion 1.0.0
   * @apiName changeProviderStatus
   * @apiGroup Services
   * @apiPermission Admin
   * @apiDescription Change status of a Service Provider
   * @apiParam {String} status
   * @apiParam {String} providerId NOTE: You must pass it in the URL itself [MANDATORY]
   */

apiRoutes.put('/changeProviderStatus/:providerId', auth('Admin'), function (req, res) {
  var status = req.body.status;
  var providerId = req.params.providerId;

  ServiceProvider.findByIdAndUpdate(providerId, {status: status}, {new: true}, function (err, provider) {
    if (err) {
      console.log(err);
      return res.json({
        error: true,
        message: "Could not be changed",
        reason: err
      });
    } else {
      res.json({
        error: false,
        data: provider
      });
    }
  });
});

/**
   * @api {delete} /deleteProvider/:providerId Delete a Service Provider
   * @apiVersion 1.0.0
   * @apiName deleteProvider
   * @apiGroup Services
   * @apiPermission Admin
   * @apiDescription Delete a Service Provider
   * @apiParam {String} providerId NOTE: You must pass it in the URL itself [MANDATORY]
   */

apiRoutes.delete('/deleteProvider/:providerId', auth('Admin'), function (req, res) {
  var providerId = req.params.providerId;
  ServiceProvider.findByIdAndRemove(providerId, function (err) {
    if (err) {
        console.log(err);
        return res.json({
            error: true,
            message: "Could not be deleted",
            reason: err
        });
    } else {
        res.json({
            error: false,
            data: "Successfully deleted"
        });
    }
  });
});

/**
   * @api {put} /updateResidentService Update a Resident's Service
   * @apiVersion 1.0.0
   * @apiName updateResidentService
   * @apiGroup Services
   * @apiPermission Admin, Resident
   * @apiDescription Update a Resident's Service
   * @apiParam {Object} services An array of Service _id-s
   * @apiParam {String} userId ID of the Resident. NOTE: You must pass it in the URL itself [MANDATORY]
   */

apiRoutes.put('/updateResidentService', auth(['Admin', 'Resident']), function (req, res) {
  var userId = req.params.userId;
  var updater = {
    services: req.body.services
  };
  User.findByIdAndUpdate(userId, updater , {new: true}, function (err, user){
    if(err){
      return res.json({
        error: true,
        message: "Could not be updated",
        reason: err
      });
    } else {
      res.json({
        error: false,
        data: user
      });
    }
  });
});

/**
   * @api {get} /getReisdentServices/:userId Get list of a Resident's Services
   * @apiVersion 1.0.0
   * @apiName getReisdentServices
   * @apiGroup Services
   * @apiPermission Admin, Resident
   * @apiDescription Get list of a Resident's Services
   * @apiParam {String} userId _id of the Resident. NOTE: You must pass it in the URL itself [MANDATORY]
   */

apiRoutes.get('/getReisdentServices/:userId', auth(['Admin', 'Resident']), function (req, res) {
  var userId = req.params.userId;
  User.findById(userId, function (err, user) {
    if(err){
      console.log(err);
      return res.json({
        error: true,
        message: "Could not get any result",
        reason: err
      });
    } else {
      res.json({
        error: false,
        data: user.services
      });
    }
  });
});

/**
   * @api {post} /createServiceRequest Create a new Service Request
   * @apiVersion 1.0.0
   * @apiName createServiceRequest
   * @apiGroup Services
   * @apiPermission Admin, Resident
   * @apiDescription Create a new Service Request
   * @apiParam {Date} date
   * @apiParam {String} requestedBy _id of the Resident
   * @apiParam {String} [serviceType] _id of a Service
   * @apiParam {String} [description]
   * @apiParam {String} [preferredSchedule]
   * @apiParam {String} [amount]
   * @apiParam {String} [timeAllocated]
   * @apiParam {String} [status="Pending"] Either "pending" or "Resolved"
   * @apiParam {String} [providerAllocated] _id of a Service Provider
   * @apiParam {String} societyId
   */

apiRoutes.post('/createServiceRequest', auth(['Admin', 'Resident']), function (req, res) {
  var servicerequest = new ServiceRequest(req.body);
  servicerequest.save(function (err) {
    if(err){
      console.log(err);
      return res.json({
        error: true,
        message: "Could not be saved",
        reason: err
      });
    } else  {
      res.json({
        error: false,
        data: {
          id: servicerequest._id
        }
      });
    }
  });
});

/**
   * @api {get} /getResidentServiceRequests/:userId Get list of Services requested by a Resident
   * @apiVersion 1.0.0
   * @apiName getResidentServiceRequests
   * @apiGroup Services
   * @apiPermission Admin, Resident
   * @apiDescription Get list of Services requested by a Resident
   * @apiParam {String} userId _id of the Resident. NOTE: You must pass it in the URL itself [MANDATORY]
   */

apiRoutes.get('/getResidentServiceRequests/:userId', auth(['Admin', 'Resident']), function (req, res) {
  var userId = req.params.userId;
  ServiceRequest
    .find({requestedBy: userId})
    .populate('providerAllocated')
    .populate('serviceType')
    .exec(function (err, results) {
      if(err){
        console.log(err);
        return res.json({
          error: true,
          message: "Could not be saved",
          reason: err
        });
      } else {
        res.json({
          error: false,
          data: results
        });
      }
    });
});

/**
   * @api {get} /getSocietyServiceRequests/:societyId Get list of all Services requested for a Society
   * @apiVersion 1.0.0
   * @apiName getSocietyServiceRequests
   * @apiGroup Services
   * @apiPermission Admin, Resident
   * @apiDescription Get list of all Services requested for a Society
   * @apiParam {String} societyId NOTE: You must pass it in the URL itself [MANDATORY]
   */

apiRoutes.get('/getSocietyServiceRequests/:societyId', auth(['Admin', 'Resident']), function (req, res) {
  var societyId = req.params.societyId;
  ServiceRequest
  .find({societyId: societyId})
  .populate('requestedBy', 'name block flatNo')
  .populate('providerAllocated')
  .exec(function (err, results) {
    if (err) {
      console.log(err);
      return res.json({
        error: true,
        message: "Could not find any service requests",
        reason: err
      });
    } else {
      if (results.length == 0) {
        return res.json({
          error: true,
          message: "Could not find any service requests"
        });
      } else {
        res.json({
          error: false,
          data: results
        });
      }
    }
  });
});

/**
   * @api {put} /updateSocietyServiceRequest/:serviceRequestId Update a Service Request
   * @apiVersion 1.0.0
   * @apiName updateSocietyServiceRequest
   * @apiGroup Services
   * @apiPermission Admin, Resident
   * @apiDescription Update a Service Request
   * @apiParam {String} serviceRequestId NOTE: You must pass it in the URL itself [MANDATORY]
   */

apiRoutes.put('/updateSocietyServiceRequest/:serviceRequestId', auth(['Admin', 'Resident']), function (req, res) {
  var serviceRequestId = req.params.serviceRequestId;
  ServiceRequest.findByIdAndUpdate(serviceRequestId, req.body, {new: true}, function (err, serviceRequest) {
    if(err){
      return res.json({
        error: true,
        message: "Could not be updated",
        reason: err
      });
    } else {
      res.json({
        error: false,
        data: serviceRequest
      });
    }
  });
});




/********** @sayanriju 26/08/16 ***********/
/**
   * @api {get} /searchResidents/:societyId Search Residents in a Society
   * @apiVersion 1.0.0
   * @apiName searchResidents
   * @apiGroup Manage Residents
   * @apiPermission Admin, Resident

   * @apiParam {String} societyId Note: You've to mention this in the URL itself [Mandatory]

   * @apiParam {String} [name] Filter residents by this name
   * @apiParam {String} [profession] Filter residents by this profession
   * @apiParam {String} [blockno] Filter residents by this block no
   * @apiParam {String} [flatno] Filter residents by this flat number
   * @apiParam {String} [carno] Filter residents by this car number


   * @apiSuccess {Object} Resident Details of the Resident

  @apiSuccessExample {json} Success Response
  {
    "error": false,
    "data": [
      {
        "_id": "57babbdd2d3b23f9334caf92",
        "name": "UDAY R KAMAT",
        "contactEmail": "meenaxe72@yahoo.com",
        "contactPhone": "9873815191",
        "societyId": "57b6d30246d40638a6d4c16b",
        "password": "$2a$10$LHQAMWPY36QPTJfMKmLkTuoD37/6ZONaJvFUhIF65mIKIXyDd6Oey",
        "__v": 0,
        "myServices": [],
        "isResiding": true,
        "residentType": "Registered Owner",
        "isProfilePrivate": false,
        "profilePicKey": "",
        "profilePicURL": "",
        "parkingSpace3": "",
        "carNumber3": "",
        "parkingSpace2": "",
        "carNumber2": "",
        "parkingSpace1": "",
        "carNumber1": "",
        "designation": "",
        "organization": "",
        "profession": "",
        "flatNo": "00-101",
        "block": "A",
        "status": "active",
        "role": "Resident",
        "awsProfilePicURL": "https://s3-ap-southeast-1.amazonaws.com/bucket-for-images/57babbdd2d3b23f9334caf92",
        "id": "57babbdd2d3b23f9334caf92"
      },
      {
        "_id": "57babbdd2d3b23f9334caf93",
        "name": "Rajiv Sharma",
        "contactEmail": "rajiv.sharma@flour.com",
        "contactPhone": "9811803465",
        "societyId": "57b6d30246d40638a6d4c16b",
        "password": "$2a$10$ocA1nkCQQtEj0nNOEm1hb.TrYYCqHdlhhFpoB2UAanO7q7ui8zdC.",
        "__v": 0,
        "myServices": [],
        "isResiding": true,
        "residentType": "Registered Owner",
        "isProfilePrivate": false,
        "profilePicKey": "",
        "profilePicURL": "",
        "parkingSpace3": "",
        "carNumber3": "",
        "parkingSpace2": "",
        "carNumber2": "",
        "parkingSpace1": "",
        "carNumber1": "",
        "designation": "",
        "organization": "",
        "profession": "",
        "flatNo": "00-124",
        "block": "A",
        "status": "active",
        "role": "Resident",
        "awsProfilePicURL": "https://s3-ap-southeast-1.amazonaws.com/bucket-for-images/57babbdd2d3b23f9334caf93",
        "id": "57babbdd2d3b23f9334caf93"
      }
    ]
  }
   */

   apiRoutes.get('/searchResidents/:societyId', auth(['Admin', 'Resident']), function (req, res) {
     var societyId = req.params.societyId;
     var filter = {"societyId": societyId};
     var limit = 50; // If NO filters, show just 50

     if (req.query.name) {
       filter.name = new RegExp(req.query.name, 'i');
       limit = null; // If a filter is active, there's no limit!
     }
     if (req.query.profession) {
       filter.profession = new RegExp(req.query.profession, 'i');
       limit = null; // If a filter is active, there's no limit!
     }
     if (req.query.blockno) {
       filter.block = new RegExp(req.query.blockno, 'i');
       limit = null; // If a filter is active, there's no limit!
     }
     if (req.query.flatno) {
       filter.flatNo = new RegExp(req.query.flatno, 'i');
       limit = null; // If a filter is active, there's no limit!
     }
     if (req.query.carno) {
       var carNo = req.query.carno;
       filter.$or = [{'carNumber1': carNo}, {'carNumber2': carNo}, {'carNumber3': carNo}];
       limit = null; // If a filter is active, there's no limit!
     }

     User.find(filter)
       .limit(limit)
       .exec(function (err, residents) {
         if (err) {
           res.json({
             error: true,
             reason: err
           })
         }
         res.json({
           error: false,
           data: residents
         });
       })
   })

/**
   * @api {post} /addResident Add new Resident to Society
   * @apiVersion 1.0.0
   * @apiName addResident
   * @apiGroup Manage Residents
   * @apiPermission Admin

   * @apiParam {String} name
   * @apiParam {String} [password] If not mentioned, one will be randomly generated
   * @apiParam {String} contactEmail
   * @apiParam {String} [contactPhone]
   * @apiParam {String} [contactAlter]
   * @apiParam {String} block=''
   * @apiParam {String} flatNo=''
   * @apiParam {String} [profession]
   * @apiParam {String} [organization]
   * @apiParam {String} [designation]
   * @apiParam {String} [carNum1]
   * @apiParam {String} [parkingSpace1]
   * @apiParam {String} [carNum2]
   * @apiParam {String} [parkingSpace2]
   * @apiParam {String} [carNum3]
   * @apiParam {String} [parkingSpace3]
   * @apiParam {Boolean} [isProfilePrivate=false]

   * @apiSuccess {Object} Resident Details of the newly added Resident

  @apiSuccessExample {json} Success Response
  {
    "error": false,
    "data": {
      "name": "Foo Bar",
      "contactEmail": "foobar@gmail.com",
      "password": "$2a$10$DXhR4pIdaANuqCUXCEluneYCY4e9ycDDq.AS94fiIpihQ.4ui3GTu",
      "societyId": "57b6d30246d40638a6d4c16b",
      "_id": "57bffb6cb6969f692adccd1e",
      "myServices": [],
      "isResiding": true,
      "residentType": null,
      "isProfilePrivate": false,
      "profilePicKey": "",
      "profilePicURL": "",
      "parkingSpace3": "",
      "carNumber3": "",
      "parkingSpace2": "",
      "carNumber2": "",
      "parkingSpace1": "",
      "carNumber1": "",
      "designation": "",
      "organization": "",
      "profession": "Not Mentioned",
      "flatNo": "",
      "block": "",
      "status": "active",
      "role": "Resident",
      "awsProfilePicURL": "https://s3-ap-southeast-1.amazonaws.com/bucket-images/57bffb6cb6969f692adccd1e",
      "id": "57bffb6cb6969f692adccd1e"
    }

   */
apiRoutes.post('/addResident', auth('Admin'), function (req, res) {
  var data = req.body;

  data.societyId = res.locals.user.societyId;
  data.addedBy = res.locals.user.id;
  data.role = 'Resident';

  var plainPass = randomString.generate(6); // a random generated password (plaintext)
  data.password = (data.password === undefined) ? plainPass : data.password;

  var resident = new User(data);
  resident.save(function(err) {
    if (err) {
      console.log(err);
      return res.json({
        error: true,
        message: "Fill the mandatory fields"
      });
    } else {
      // send password by email
      var locals = {
        to: data.contactEmail,
        // to: 's26c.sayan@gmail.com', // for testing
        subject: 'Welcome to Smart Society!',
        email: data.contactEmail,
        password: data.password,
        name: data.name
      }
      res.mailer.send('emails/password', locals, function (err) {
        if (!err)
        console.log('Password email sent successfully to %s !', data.contactEmail);
        //  res.send('done!');
        else {
          console.log("error: %o", err);
          // res.send({error: true})
        }
      });
      // don't wait for email sending to complete
      res.json({
        error: false,
        data: resident
      });
    }
  });
});


/**
   * @api {get} /getResidentDetails/:residentId Get Details of a Resident
   * @apiVersion 1.0.0
   * @apiName getResidentDetails
   * @apiGroup Manage Residents
   * @apiPermission Admin, Resident

   * @apiParam {String} residentId Note: You've to mention this in the URL itself [Mandatory]

   * @apiSuccess {Object} Resident Details of the Resident

  @apiSuccessExample {json} Success Response
  {
    "error": false,
    "data": {
      "name": "Foo Bar",
      "contactEmail": "foobar@gmail.com",
      "password": "$2a$10$DXhR4pIdaANuqCUXCEluneYCY4e9ycDDq.AS94fiIpihQ.4ui3GTu",
      "societyId": "57b6d30246d40638a6d4c16b",
      "_id": "57bffb6cb6969f692adccd1e",
      "myServices": [],
      "isResiding": true,
      "residentType": null,
      "isProfilePrivate": false,
      "profilePicKey": "",
      "profilePicURL": "",
      "parkingSpace3": "",
      "carNumber3": "",
      "parkingSpace2": "",
      "carNumber2": "",
      "parkingSpace1": "",
      "carNumber1": "",
      "designation": "",
      "organization": "",
      "profession": "Not Mentioned",
      "flatNo": "",
      "block": "",
      "status": "active",
      "role": "Resident",
      "awsProfilePicURL": "https://s3-ap-southeast-1.amazonaws.com/bucket-images/57bffb6cb6969f692adccd1e",
      "id": "57bffb6cb6969f692adccd1e"
    }
  }
   */

  apiRoutes.get('/getResidentDetails/:residentId', auth(['Admin', 'Resident']), function (req, res) {
    User.findById( req.params.residentId, function (err, data) {
        if( err) {
          console.log(err);
          return res.json({
            error: true,
            message: "Could not find the user"
          });
      } else {
        res.json({
          error: false,
          data: data
        });
      }
    });
  });

  /**
     * @api {put} /editResidentDetails/:residentId Edit some details of a Resident
     * @apiVersion 1.0.0
     * @apiName editResidentDetails
     * @apiGroup Manage Residents
     * @apiPermission Admin, Resident

     * @apiParam {String} residentId Note: You've to mention this in the URL itself [Mandatory]
     * @apiParam {String} [name]
     * @apiParam {String} [contactEmail]
     * @apiParam {String} [contactPhone]
     * @apiParam {String} [profession]
     * @apiParam {String} [organization]
     * @apiParam {String} [designation]
     * @apiParam {Boolean} [isProfilePrivate=false]

     * @apiSuccess {Object} Resident Updated Details of the Resident

    @apiSuccessExample {json} Success Response
    {
      "error": false,
      "data": {
        "name": "Changed Name",
        "contactEmail": "foobar@gmail.com",
        "password": "$2a$10$DXhR4pIdaANuqCUXCEluneYCY4e9ycDDq.AS94fiIpihQ.4ui3GTu",
        "societyId": "57b6d30246d40638a6d4c16b",
        "_id": "57bffb6cb6969f692adccd1e",
        "myServices": [],
        "isResiding": true,
        "residentType": null,
        "isProfilePrivate": false,
        "profilePicKey": "",
        "profilePicURL": "",
        "parkingSpace3": "",
        "carNumber3": "",
        "parkingSpace2": "",
        "carNumber2": "",
        "parkingSpace1": "",
        "carNumber1": "",
        "designation": "",
        "organization": "",
        "profession": "Not Mentioned",
        "flatNo": "",
        "block": "",
        "status": "active",
        "role": "Resident",
        "awsProfilePicURL": "https://s3-ap-southeast-1.amazonaws.com/bucket-images/57bffb6cb6969f692adccd1e",
        "id": "57bffb6cb6969f692adccd1e"
      }

     */

  apiRoutes.put('/editResidentDetails/:residentId', auth(['Admin', 'Resident']), function (req, res) {
    delete req.body.password;
    var residentId = req.params.residentId;
    User.findByIdAndUpdate(residentId, req.body, {new: true}, function (err, resident) {
      if (err) {
        console.log(err);
        return res.json({
          error: true,
          message: 'Update was not possible',
          reason: err
        })
      } else {
        res.json({
          error: false,
          data: resident
        });
      }
    })
  });

  /** @sayanriju 01/09/2016 **/

  /**
     * @api {get} /getSubscribedModules/:societyId Get the Feature/Modules made available for this society
     * @apiVersion 1.0.0
     * @apiName getSubscribedModules
     * @apiGroup Manage Societies
     * @apiPermission Anyone

     *@apiDescription Retrieve the list of all Feature/Modules enabled for this society

     * @apiParam {String} societyId Note: You've to mention this in the URL itself [Mandatory]


     * @apiSuccess {Object} Feature/Modules made available for this society
    @apiSuccessExample {json} Success Response
    {
      error: false,
      data: {
        societyId: "57b6d30246d40638a6d4c16b",
        modules: {
          payments: true,
          members: true,
          classifieds: true,
          contacts: false,
          visitors: false,
          polls: false,
          documents: false,
          employees: false,
          residents: false,
          services: false
        }
      }
    }

     */
   apiRoutes.get('/getSubscribedModules/:societyId', function (req, res) {
     var societyId = req.params.societyId;
     Society.findById(societyId, function (err, society) {
       if (err || !society) {
         return res.json({error: true, reason: "No such society!"});
       }
       return res.json({
         error: false,
         data: {
           societyId: societyId,
           modules: society.modulesSubscribed
         }
       })
     })
   });

/******************** Endpoints for File UL to AWS (added 04/10/2016 @sayanriju) ***********************/
var AWS = require('aws-sdk');
var multer = require("multer");
var multerS3 = require("multer-s3");

AWS.config.update({accessKeyId: config.aws.accessKeyId, secretAccessKey: config.aws.secretAccessKey});
AWS.config.region = config.aws.region;
var s3 = new AWS.S3();
var ObjectID = require('mongodb').ObjectID;

var uploadImage = multer({
  onError: function (err, next) {
    console.log("!!!!!!!!!!!!!ERROR ",err);
    next(err);
  },
  storage: multerS3({
    s3: s3,
    bucket: config.aws.imageBucketName,
    // metadata: function (req, file, cb) {
    //   cb(null, {fieldName: file.fieldname});
    // },
    acl: 'public-read',
    key: function (req, file, cb) {
      cb(null, req.params.docid)
    }
  })
}).any();

var uploadDoc = multer({
  storage: multerS3({
    s3: s3,
    acl: 'public-read',
    bucket: config.aws.documentBucketName,
    // metadata: function (req, file, cb) {
    //   cb(null, {fieldName: file.fieldname});
    // },
    key: function (req, file, cb) {
      cb(null, String(new ObjectID()))
    }
  })
}).single('mydocument');

AWS.config.update({accessKeyId: config.aws.accessKeyId, secretAccessKey: config.aws.secretAccessKey});
AWS.config.region = config.aws.region;


/**
   * @api {post} /uploadimage/:docid Upload Image File(s) to AWS
   * @apiVersion 1.0.0
   * @apiName uploadImage
   * @apiGroup File Uploading
   * @apiPermission Anyone

   *@apiDescription Upload Image File(s) to AWS associated with a newly added or existing Mongo Doc

   * @apiParam {String} docid  This is the _id of the Mongo Document with which you need to "associate" the uploaded image(s). Note: You've to mention this in the URL itself [Mandatory]
   * @apiParam {File} myfile The File(s) to upload. !!! IMPORTANT !!! Expects a Multipart Form data


   * @apiSuccess {Object} uploadedFiles Information about the Uploaded File(s) [An Array of Objects]
  @apiSuccessExample {json} Success Response
  {
    "error": false,
    "uploadedFiles": [
      {
        "fieldname": "myfile",
        "originalname": "foobar.png",
        "encoding": "7bit",
        "mimetype": "image/png",
        "size": 2690,
        "bucket": "image-bucket-name",
        "key": "579b13562525d146251a4d6f",
        "acl": "public-read",
        "contentType": "application/octet-stream",
        "metadata": null,
        "location": "https://image-bucket-name.s3-ap-southeast-1.amazonaws.com/579b13562525d146251a4d6f",
        "etag": "\"45c1b48cfe030a3eb1e9a3abd0140387\""
      }
    ]
  }

   */
apiRoutes.post('/uploadimage/:docid', auth(['Admin', 'Resident', 'Super Admin']), function (req, res) {
  uploadImage(req, res, function (err) {
    if (err) {
      return res.json({error: true, message: "Failed to Upload Image(s) to AWS!", reason: err});
    }
    if (req.files === undefined || !req.files || req.files.length == 0) {
      return res.json({error: true, message: "No Files Chosen!"});
    }
    return res.json({error: false, uploadedFiles: req.files})
  });
})


/**
   * @api {post} /uploaddocument Upload a single Resident Document File to AWS
   * @apiVersion 1.0.0
   * @apiName uploadDocument
   * @apiGroup File Uploading
   * @apiPermission Anyone

   *@apiDescription Upload a single Resident Document File to AWS _before_ adding other metadata about the document using /addResidentDoc

   * @apiParam {File} mydocument The File to upload. !!! IMPORTANT !!! Expects a Multipart Form data


   * @apiSuccess {Object} uploadedFile Information about the Uploaded File. To be used with /addResidentDoc
  @apiSuccessExample {json} Success Response
  {
    "error": false,
    "uploadedFile": {
      "aws": {
        "key": "57f36eecfa0f429f29bd1849",
        "bucket": "document-bucketname-example",
        "fileName": "pancard.pdf"
      },
      "location": "https://document-bucketname-example.s3-ap-southeast-1.amazonaws.com/57f36eecfa0f429f29bd1849"
    }
  }

   */
apiRoutes.post('/uploaddocument', auth(['Admin', 'Resident', 'Super Admin']), function (req, res) {
  uploadDoc(req, res, function (err) {
    if (err) {
      return res.json({error: true, message: "Failed to Upload Document to AWS!", reason: err});
    }
    if (req.file === undefined || !req.file) {
      return res.json({error: true, message: "No File Chosen!"});
    }
    return res.json({
      error: false,
      uploadedFile: {
        aws: {
          key: req.file.key,
          bucket: req.file.bucket,
          fileName: req.file.originalname
        },
        location: req.file.location
      }
    })
  });
})

/**
   * @api {get} /getMyBills Get a Resident's Bills
   * @apiVersion 1.0.0
   * @apiName getMyBills
   * @apiGroup Bills
   * @apiPermission Resident

   *@apiDescription Get a Resident's Bills by Block & Flat No.

   * @apiParam {String} block The Resident's Block
   * @apiParam {String} flatNo The Resident's Flat Number


  @apiSuccessExample {json} Success Response
  {
    "error": false,
    "bills": [
      {
        "_id": "5805f7257e3e93b078432a75",
        "block": "Block A",
        "flatNo": "11",
        "dueDate": "2016-10-17T18:30:00.000Z",
        "desc": "test",
        "amount": 200,
        "societyId": "57b6d30246d40638a6d4c16b",
        "addedBy": "579b3effcbe18ca33fc06512",
        "billNo": "BILL5n5g51nu8iufc89p9",
        "__v": 0,
        "addedOn": "2016-10-18T10:18:52.136Z"
      },
      {
        "_id": "5805fbc79b58bcad7e3b7fcc",
        "block": "Block A",
        "flatNo": "11",
        "dueDate": "2016-10-17T18:30:00.000Z",
        "desc": "",
        "amount": 1,
        "societyId": "57b6d30246d40638a6d4c16b",
        "addedBy": "579b3effcbe18ca33fc06512",
        "billNo": "BILL5n5g51p0tiufcxoqf",
        "__v": 0,
        "addedOn": "2016-10-18T10:35:12.032Z"
      }
    ]
  }

   */
apiRoutes.get('/getMyBills', auth('Resident'), function (req, res) {
  var block = req.query.block; // SECURITY Note: Should ideally extract this from JWT token!
  var flatNo = req.query.flatNo;
  if (flatNo === undefined || !flatNo) {
    return res.json({error: true, message: "No Flat Number provided!"})
  }
  if (block === undefined || !block) {
    return res.json({error: true, message: "No Block provided!"})
  }
  Bill
  .find({block: block, flatNo: flatNo})
  .exec()
  .then(function (bills) {
    return res.json({
      error: false,
      bills: bills
    })
  })
  .catch(function (err) {
    return res.json({
      error: true,
      message: "Couldn't Fetch Bills!",
      reason: err
    })
  })
})


module.exports = apiRoutes;
